# 08_Mobile_Security
---------------

Mobile Security

# Gif Showcase
---------------

![Alt Text](security.gif)

## Authors
---------------

* **Andrei Golban**

## License
---------------

This project is licensed under the MIT License - see the [LICENSE.md](https://opensource.org/licenses/MIT) file for details
