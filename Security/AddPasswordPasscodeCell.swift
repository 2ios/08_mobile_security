//
//  AddPasswordPasscodeCell.swift
//  Security
//
//  Created by User543 on 16.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AddPasswordPasscodeCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var switchPass: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateViews(isSwitchOn: Bool) {
        self.backgroundColor = UIColor.clear
        title.text = "Passcode Lock"
        switchPass.isOn = isSwitchOn
    }

}
