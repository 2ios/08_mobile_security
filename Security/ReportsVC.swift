//
//  ReportsVC.swift
//  Security
//
//  Created by User543 on 05.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import LGAlertView

class ReportsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LGAlertViewDelegate {

    @IBOutlet weak var reportsTable: UITableView!
    
    var typeReports: TypeReports = TypeReports.showReports
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reportsTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
        setNavigationBar(typeReports: typeReports)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getReports().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ReportsCell") as? ReportsCell {
            let report = DataService.instance.getReports()[indexPath.row]
            var count = 0
//            let web = navigationController?.presentingViewController as? WebViewController
            if IAPHelper.instance.isSubscribed {
                if indexPath.row == 1 {
//                    if let sum = web?.curWebViewTab().applicableHTTPSEverywhereRules.count {
//                        count = sum
//                    }
                                    if let dict = UserDefaults.standard.object(forKey: "applicableHTTPSEverywhereRules") as? NSDictionary {
                                        count = dict.count
                                    }
                }
                if indexPath.row == 2 {
//                    if let sum = web?.curWebViewTab().applicableURLBlockerTargets.count {
//                        count = sum
//                    }
                                    if let dict = UserDefaults.standard.object(forKey: "applicableURLBlockerTargets") as? NSDictionary {
                                        count = dict.count
                                    }
                }
            }
            cell.updateView(report: report, count: count)
            return cell
        } else {
            return ReportsCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Check if active subscription
        if IAPHelper.instance.isSubscribed {
            let identifier = DataService.instance.getSeguesReports()[indexPath.row]
            performSegue(withIdentifier: identifier, sender: nil)
        } else {
            let alert = LGAlertView.init(title: "Reports Not Activated", message: "In order to receive reports, you need to enable security. Go back to the main screen and tap \"Fix Now\" to enable.", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: nil, destructiveButtonTitle: "OK")
            showAlertWithOneButton(alert: alert)
        }
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlertWithOneButton(alert: LGAlertView) {
        alert.delegate = self
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
    
    func setNavigationBar(typeReports: TypeReports) {
        switch typeReports {
        case .showReports:
            showReports()
        case .showFromSafeBrowser:
            showFromSafeBrowser()
        }
    }
    
    func showReports() {
        navigationItem.title = "REPORTS"
        setupNavigationBar()
    }
    
    func showFromSafeBrowser() {
        navigationItem.title = "REPORTS"
        setupNavigationBar()
        navigationItem.rightBarButtonItem = UIBarButtonItem()
        let leftBtn = UIButton(type: .custom)
        leftBtn.addTarget(self, action: #selector(backButton), for: .touchUpInside)
        if let image = UIImage(named: "nav_ico_close") {
            leftBtn.setImage(image, for: .normal)
            leftBtn.frame.size = CGSize(width: image.size.width, height: image.size.height)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBtn)
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 0.5607843137, green: 0.6156862745, blue: 0.6666666667, alpha: 1)
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2039215686, green: 0.3843137255, blue: 0.5176470588, alpha: 1)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func backButton() {
        self.dismiss(animated: true, completion: nil)
    }
}
