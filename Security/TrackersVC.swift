//
//  TrackersVC.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class TrackersVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var trackersTable: UITableView!
    
    var trackersDict = NSDictionary()
    var keyTrackersDict = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        trackersTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
        if let dict = UserDefaults.standard.object(forKey: "applicableHTTPSEverywhereRules") as? NSDictionary {
            trackersDict = dict
            keyTrackersDict = trackersDict.allKeys as! [String]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trackersDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TrackersCell") as? TrackersCell {
            let title = keyTrackersDict[indexPath.row]
            let date = trackersDict[title]
            let trackers = Trackers(title: title as! String, date: date as! String)
            cell.updateViews(trackers: trackers)
            return cell
        } else {
            return TrackersCell()
        }
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func clearBtnPressed(_ sender: Any) {
        trackersDict = [:]
        trackersTable.reloadData()
        UserDefaults.standard.set(trackersDict, forKey: "applicableHTTPSEverywhereRules")
        UserDefaults.standard.synchronize()
    }
}
