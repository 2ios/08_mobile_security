//
//  WebViewController.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <InAppSettingsKit/IASKAppSettingsViewController.h>
#import "WebViewTab.h"
#import <WYPopoverController/WYPopoverController.h>

#define TOOLBAR_HEIGHT 47
#define TOOLBAR_PADDING 6
#define TOOLBAR_BUTTON_SIZE 30

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]

@interface WebViewController : UIViewController <UITableViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, IASKSettingsDelegate, WYPopoverControllerDelegate>

@property BOOL toolbarOnBottom;
@property BOOL darkInterface;

- (void)focusUrlField;

- (NSMutableArray *)webViewTabs;
- (__strong WebViewTab *)curWebViewTab;

- (UIButton *)settingsButton;

- (void)viewIsVisible;
- (void)viewIsNoLongerVisible;

- (WebViewTab *)addNewTabForURL:(NSURL *)url;
- (WebViewTab *)addNewTabForURL:(NSURL *)url forRestoration:(BOOL)restoration withCompletionBlock:(void(^)(BOOL))block;
- (void)switchToTab:(NSNumber *)tabNumber;
- (void)removeTab:(NSNumber *)tabNumber andFocusTab:(NSNumber *)toFocus;
- (void)removeTab:(NSNumber *)tabNumber;
- (void)removeAllTabs;

- (void)webViewTouched;
- (void)updateProgress;
- (void)updateSearchBarDetails;
- (void)refresh;
- (void)forceRefresh;
- (void)dismissPopover;
- (void)prepareForNewURLFromString:(NSString *)url;
- (void)showBookmarksForEditing:(BOOL)editing;
- (void)hideBookmarks;

@end
