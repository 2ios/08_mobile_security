//
//  URLBlocker.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLBlocker : NSObject

+ (NSDictionary *)targets;
+ (NSMutableDictionary *)disabledTargets;
+ (void)saveDisabledTargets;

+ (BOOL)shouldBlockURL:(NSURL *)url;
+ (NSString *)blockingTargetForURL:(NSURL *)url fromMainDocumentURL:(NSURL *)mainUrl;
+ (void)enableTargetByHost:(NSString *)target;
+ (void)disableTargetByHost:(NSString *)target withReason:(NSString *)reason;

@end
