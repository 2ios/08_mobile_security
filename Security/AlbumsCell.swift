//
//  AlbumsCell.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AlbumsCell: UITableViewCell {
    
    @IBOutlet weak var albumCoverImageView: UIImageView!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var lockedAlbum: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    
    func updateViews(album: Album) {
        if let image = album.albumCoverImage as? UIImage {
            albumCoverImageView.image = image
        }
        if album.albumName == "" {
            albumName.text = "No title"
        } else {
            albumName.text = album.albumName
        }
        if album.albumPasscode != "" {
            lockedAlbum.isHidden = false
        } else {
            lockedAlbum.isHidden = true
        }
    }
}
