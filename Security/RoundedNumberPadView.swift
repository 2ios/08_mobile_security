//
//  RoundedNumberPadView.swift
//  Security
//
//  Created by User543 on 08.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

//@IBDesignable
class RoundedNumberPadView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = cornerRadius
    }

}
