//
//  AntiTheftCell.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AntiTheftCell: UITableViewCell {

    @IBOutlet weak var antiTheftImage: UIImageView!
    @IBOutlet weak var antiTheftName: UILabel!
    @IBOutlet weak var antiTheftInfo: UILabel!
    @IBOutlet weak var statusIndicator: RoundedView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    
    func updateViews(antiTheft: AntiTheft) {
        antiTheftImage.image = UIImage(named: antiTheft.imageName)
        antiTheftName.text = antiTheft.title
        antiTheftInfo.text = antiTheft.info
    }
    
}
