//
//  MainVC.swift
//  Security
//
//  Created by User543 on 05.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import LGAlertView
import PulsingHalo
import SwiftyStoreKit
import CoreMotion
import AVFoundation
import AudioToolbox

class MainVC: UIViewController, LGAlertViewDelegate {
    
    //Variables
    let motionManager = CMMotionManager()
    var halo: PulsingHaloLayer?
    var timer: Timer?
    var sound: SystemSoundID = 0
    
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var shieldImageView: UIImageView!
    @IBOutlet weak var secureLbl: UILabel!
    @IBOutlet weak var protectedLbl: UILabel!
    @IBOutlet weak var fixLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        motionManager.startAccelerometerUpdates()
        NotificationCenter.default.addObserver(self, selector: #selector(motion), name: NSNotification.Name(rawValue: "motion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stopAlarm), name: NSNotification.Name(rawValue: "stopAlarm"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(power), name: NSNotification.Name.UIDeviceBatteryStateDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(headset), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
        checkFirstTime()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startHaloPulse()
        halo!.position = CGPoint(x: shieldImageView.layer.frame.width/2, y: shieldImageView.layer.frame.height/2)
        checkSubscription()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2039215686, green: 0.3843137255, blue: 0.5176470588, alpha: 1)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func checkFirstTime() {
//        if UserDefaults.standard.object(forKey: "firstTime") == nil {
//            UserDefaults.standard.set(true, forKey: "firstTime")
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            if let numPadVC = storyboard.instantiateViewController(withIdentifier: NUMBER_PAD) as? NumberPadVC {
//                numPadVC.typeNumberPad = .newPassword
//                let navigation = UINavigationController(rootViewController: numPadVC)
//                self.present(navigation, animated: false, completion: nil)
//            }
//        } else {
            if !IAPHelper.instance.isSubscribed {
              showAlertFreeTrial()
            }
//        }
    }
    
    func checkSubscription() {
        if IAPHelper.instance.isSubscribed {
            self.shieldImageView.image = UIImage(named: "connected")
            self.secureLbl.text = "SECURED"
            self.protectedLbl.text = "You're fully protected"
            self.fixLbl.alpha = 0
        } else {
            self.shieldImageView.image = UIImage(named: "disconnected")
            self.secureLbl.text = "UNSECURED"
            self.protectedLbl.text = "You're not fully protected"
            self.fixLbl.alpha = 1
        }
    }
    
    func buySubscription() {
        IAPHelper.instance.purchase(.autoRenewablePurchase, vc: self) { (success) in
            if success {
                self.shieldImageView.image = UIImage(named: "connected")
                self.secureLbl.text = "SECURED"
                self.protectedLbl.text = "You're fully protected"
                self.fixLbl.alpha = 0
                print("Success buy subscription")
            } else {
                print("No success buy subscription")
            }
        }
    }

    @IBAction func fixNowBtnPressed(_ sender: Any) {
        buySubscription()
    }

    @IBAction func safeBrowserBtnPressed(_ sender: Any) {
        let webVC = WebViewController()
        self.present(webVC, animated: false, completion: nil)
    }
    
    func showAlertFreeTrial() {
        let alert = LGAlertView(title: "Start Your Free Trial", message: "You can instantly enjoy free Premium Security to protect your connection in all apps and cancel at any time", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "NO, THANKS", destructiveButtonTitle: "CONTINUE", delegate: self)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.buySubscription()}
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func startHaloPulse() {
        halo = PulsingHaloLayer()
        shieldImageView.layer.insertSublayer(halo!, at: 0)
        halo!.radius = shieldImageView.frame.height / 2
        halo!.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        halo!.shouldResume = true
        halo!.start()
    }
    
    func motion() {
        var activate = true
        motionManager.gyroUpdateInterval = 0.3
        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data, error) in
            guard let data  = data else { return }
            if (data.rotationRate.x < -1 || data.rotationRate.x > 1) {
                if activate {
                    self.showNumberPad()
                    UserDefaults.standard.set(true, forKey: "activeMotion")
                    UserDefaults.standard.synchronize()
                    activate = false
                }
            }
        }
    }
    
    func power() {
        let state = UIDevice.current.batteryState
        if state == .unplugged || state == .unknown {
            showNumberPad()
            UserDefaults.standard.set(true, forKey: "activePower")
            UserDefaults.standard.synchronize()
        }
    }
    
    func headset() {
        let route  = AVAudioSession.sharedInstance().currentRoute;
        for desc in route.outputs
        {
            let portType = desc.portType;
            if (portType != AVAudioSessionPortHeadphones)
            {
                showNumberPad()
                UserDefaults.standard.set(true, forKey: "activeHeadset")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func showNumberPad() {
        startAlarm()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let numPadVC = storyboard.instantiateViewController(withIdentifier: NUMBER_PAD) as? NumberPadVC {
            numPadVC.typeNumberPad = .alert
            let navigation = UINavigationController(rootViewController: numPadVC)
            if let topVC = getTopViewController(self) {
                topVC.present(navigation, animated: false, completion: nil)
            }
        }
    }
    
    func startAlarm() {
        if let soundURL = Bundle.main.url(forResource: "alarm", withExtension: "caf") {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &sound)
            AudioServicesPlaySystemSound(sound)
            startTimer()
        }
    }
    
    func stopAlarm() {
        AudioServicesDisposeSystemSoundID(sound)
        stopTimer()
    }
    
    func startTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.loop), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    func loop() {
       AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
}

