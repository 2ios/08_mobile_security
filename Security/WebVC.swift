//
//  WebVC.swift
//  Security
//
//  Created by User543 on 10.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class WebVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    
    var item = 0
    var navTitle = ""

    let urlArray = [
        "http://devios.mobi/max_fm_res/Mobile_Security/tou_MobileSecurity.htm",
        "http://devios.mobi/max_fm_res/Mobile_Security/privacy_MobileSecurity.htm",
        "http://devios.mobi/max_fm_res/Mobile_Security/subscription_MobileSecurity.htm"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
        navigationItem.title = navTitle
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        LoadText()
    }
    
    
    func LoadText() {
        guard let url = URL(string: urlArray[item]) else {
            print("Error: \(urlArray[item]) doesn't seem to be a valid URL")
            return
        }
        do {
            let data = try Data(contentsOf: url)
            let str = try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
            textView.attributedText = str
            textView.textStorage.addAttributes([NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 18)], range: NSRange(location: 0, length: textView.attributedText.length))
        } catch let error {
            print("Error: \(error)")
        }
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termOfUseBtnPressed(_ sender: Any) {
        item = 0
        LoadText()
    }
    
    @IBAction func privacyPolicyBtnPressed(_ sender: Any) {
        item = 1
        LoadText()
    }
    
    @IBAction func subscriptionInfoBtnPressed(_ sender: Any) {
        item = 2
        LoadText()
    }
}
