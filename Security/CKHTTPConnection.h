//
//  CKHTTPConnection.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSLCertificate.h"

@protocol CKHTTPConnectionDelegate;

@class CKHTTPAuthenticationChallenge;

@interface CKHTTPConnection : NSObject
{
@private
    __weak id <CKHTTPConnectionDelegate> _delegate;
    
    CFHTTPMessageRef _HTTPRequest;
    NSInputStream *_HTTPStream;
    NSInputStream *_HTTPBodyStream;
    BOOL _haveReceivedResponse;
    CKHTTPAuthenticationChallenge *_authenticationChallenge;
    NSInteger _authenticationAttempts;
    
    BOOL socketReady;
    BOOL retriedSocket;
}

+ (CKHTTPConnection *)connectionWithRequest:(NSURLRequest *)request delegate:(id <CKHTTPConnectionDelegate>)delegate;
- (id)initWithRequest:(NSURLRequest *)request delegate:(id <CKHTTPConnectionDelegate>)delegate;
- (void)cancel;

@end


@protocol CKHTTPConnectionDelegate

- (void)HTTPConnection:(CKHTTPConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
- (void)HTTPConnection:(CKHTTPConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
- (void)HTTPConnection:(CKHTTPConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;

- (void)HTTPConnection:(CKHTTPConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response;
- (void)HTTPConnection:(CKHTTPConnection *)connection didReceiveData:(NSData *)data;

- (void)HTTPConnection:(CKHTTPConnection *)connection didReceiveSecTrust:(SecTrustRef)secTrustRef certificate:(SSLCertificate *)certificate;

- (void)HTTPConnectionDidFinishLoading:(CKHTTPConnection *)connection;
- (void)HTTPConnection:(CKHTTPConnection *)connection didFailWithError:(NSError *)error;

@end


@interface NSURLRequest (CKHTTPURLRequest)
- (CFHTTPMessageRef)makeHTTPMessage;
@end

