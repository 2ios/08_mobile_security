//
//  ContactsCell.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class ContactsCell: UITableViewCell {
    
    @IBOutlet weak var contactCoverImageView: UIImageView!
    @IBOutlet weak var contactTitle: UILabel!
    @IBOutlet weak var lockedContact: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    
    func updateViews(contact: Contact) {
        if let image = contact.contactImage {
            contactCoverImageView.image = image as? UIImage
        }
        if contact.contactFirstName == "" {
            contactTitle.text = "No firstname"
        } else {
            contactTitle.text = contact.contactFirstName 
        }
        if contact.contactPasscode != "" {
            lockedContact.isHidden = false
        } else {
            lockedContact.isHidden = true
        }
    }
}
