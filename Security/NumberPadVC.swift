//
//  NumberPadVC.swift
//  Security
//
//  Created by User543 on 08.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import LGAlertView
import AudioToolbox

protocol NumberPadVCDelegate: NSObjectProtocol {
    func removeNumberPad() -> Void
}

class NumberPadVC: UIViewController, LGAlertViewDelegate {
    
    //Variables
    weak var delegate: NumberPadVCDelegate?
    var typeNumberPad = TypeNumberPad.checkPassword
    var password = ""
    var step = 0
    
    //Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var circles: [CircleNumberPadView]!
    
    var navigation: NavigationNumberPad!
    var numberPressedBtn = -1
    var inputPass = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2039215686, green: 0.3843137255, blue: 0.5176470588, alpha: 1)
        if typeNumberPad == .newPassword || typeNumberPad == .foreground || typeNumberPad == .alert {
            navigation = NavigationNumberPad(title: "", nameImageLeftBtn: "", nameImageRightBtn: "", titleLeftBtn: "", titleRightBtn: "", emptyLeftBtn: true, emptyRightBtn: true)
            (navigationItem as! NavigationNumberPadView).updateViews(navigation: navigation, selectorLeftBtn: nil, selectorRightBtn: nil, target: self)
        }
        if typeNumberPad == .repeatNewPassword {
            navigation = NavigationNumberPad(title: "", nameImageLeftBtn: "nav_back_button", nameImageRightBtn: "", titleLeftBtn: "", titleRightBtn: "", emptyLeftBtn: false, emptyRightBtn: true)
            (navigationItem as! NavigationNumberPadView).updateViews(navigation: navigation, selectorLeftBtn: #selector(backNavigationBtn), selectorRightBtn: nil, target: self)
        }
    }
    
    func backNavigationBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if typeNumberPad == .newPassword {
            titleLbl.text = "CREATE YOUR PIN"
        }
        if typeNumberPad == .repeatNewPassword {
            titleLbl.text = "YOUR CURRENT PIN"
        }
    }

    @IBAction func numBtnPressed(_ sender: Any) {
        catchPassword(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func clearBtnPressed(_ sender: Any) {
        if numberPressedBtn > -1 {
            circles[numberPressedBtn].alpha = 0
            numberPressedBtn -= 1
        }
    }
    
    func catchPassword(tag: Int) {
        numberPressedBtn += 1
        circles[numberPressedBtn].alpha = 1
        password += "\(tag)"
        if numberPressedBtn == 3 {
            numberPressedBtn = -1
            switch typeNumberPad {
            case .newPassword:
                createNewPassword()
            case .checkPassword, .alert, .foreground:
                let when = DispatchTime.now() + 0.1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.checkPassword()
                }
            case .repeatNewPassword:
                let when = DispatchTime.now() + 0.1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.repeatNewPassword()
                }
            }
        }
    }
    
    func createNewPassword() {
        let alert = LGAlertView(title: "Your password is", message: "\(password)", style: LGAlertViewStyle.alert, buttonTitles: ["Change password"], cancelButtonTitle: "", destructiveButtonTitle: "OK, let's go!", delegate: self)
        alert.didDismissAfterActionHandler = changePasswordAction(_:didDismissAfterClickedButtonAt:title:)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.savePassword()}
        self.showAlertWithTwoButtons(alert: alert)
    }
    
    func checkPassword() {
        if let pass = UserDefaults.standard.value(forKey: "password") as? String {
            if password == pass {
                if let numberPadDelegate = delegate {
                    numberPadDelegate.removeNumberPad()
                } else {
                    if typeNumberPad == .alert {
                        NotificationCenter.default.post(name: NSNotification.Name.init("stopAlarm"), object: nil)
                        let motion = UserDefaults.standard.bool(forKey: "activeMotion")
                        let power = UserDefaults.standard.bool(forKey: "activePower")
                        let headset = UserDefaults.standard.bool(forKey: "activeHeadset")
                        if motion {
                            UserDefaults.standard.set(false, forKey: "motion")
                        }
                        if power {
                            UserDefaults.standard.set(false, forKey: "power")
                        }
                        if headset {
                            UserDefaults.standard.set(false, forKey: "headset")
                        }
                        UserDefaults.standard.synchronize()
                    }
                    self.dismiss(animated: false, completion: nil)
                }
            } else {
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                let alert = LGAlertView(title: "Try again", message: "Your input password is wrong", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: nil, destructiveButtonTitle: nil, delegate: self)
                alert.didShowHandler = {[unowned self] (alertView: LGAlertView) in self.dismissAlert(alert: alert)}
                alert.showAnimated()
                changePassword()
            }
        }
    }
    
    func repeatNewPassword() {
        if step == 0 {
            if let pass = UserDefaults.standard.value(forKey: "password") as? String {
                if password == pass {
                    titleLbl.text = "CREATE YOUR PIN"
                    step = 1
                } else {
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    let alert = LGAlertView(title: "Try again", message: "Your input password is wrong", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: nil, destructiveButtonTitle: nil, delegate: self)
                    alert.didShowHandler = {[unowned self] (alertView: LGAlertView) in self.dismissAlert(alert: alert)}
                    alert.showAnimated()
                    titleLbl.text = "YOUR CURRENT PIN"
                    step = 0
                }
                changePassword()
            }
        } else {
            createNewPassword()
        }
    }
    
    func showAlertWithTwoButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
    
    func savePassword() {
        UserDefaults.standard.set(password, forKey: "password")
        if typeNumberPad == .newPassword {
            self.dismiss(animated: false, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func changePassword() {
        password = ""
        for circle in circles {
            circle.alpha = 0
        }
    }
    
    func dismissAlert(alert: LGAlertView){
        alert.dismissAnimated()
        changePassword()
    }
    
    func changePasswordAction(_ alertView: LGAlertView, didDismissAfterClickedButtonAt index: UInt, title: String?) {
        changePassword()
        if typeNumberPad == .repeatNewPassword {
            titleLbl.text = "YOUR CURRENT PIN"
            step = 0
        }
    }
}
