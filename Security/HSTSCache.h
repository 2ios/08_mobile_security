//
//  HSTSCache.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HSTS_HEADER @"Strict-Transport-Security"
#define HSTS_KEY_EXPIRATION @"expiration"
#define HSTS_KEY_ALLOW_SUBDOMAINS @"allowSubdomains"
#define HSTS_KEY_PRELOADED @"preloaded"

/* subclassing NSMutableDictionary is not easy, so we have to use composition */

@interface HSTSCache : NSObject
{
    NSMutableDictionary *_dict;
}

@property NSMutableDictionary *dict;

+ (HSTSCache *)retrieve;

- (void)persist;
- (NSURL *)rewrittenURI:(NSURL *)URL;
- (void)parseHSTSHeader:(NSString *)header forHost:(NSString *)host;

/* NSMutableDictionary composition pass-throughs */
- (id)objectForKey:(id)aKey;
- (BOOL)writeToFile:(NSString *)path atomically:(BOOL)useAuxiliaryFile;
- (void)setValue:(id)value forKey:(NSString *)key;
- (void)removeObjectForKey:(id)aKey;
- (NSArray *)allKeys;

@end

