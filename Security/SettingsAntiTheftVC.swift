//
//  SettingsAntiTheftVC.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class SettingsAntiTheftVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var settingsAntiTheftTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsAntiTheftTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DataService.instance.getHeaderSettingsAntiTheft().count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getSettingsAntiTheft().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsAntiTheftCell") as? SettingsAntiTheftCell {
            let settingsAntiTheft = DataService.instance.getSettingsAntiTheft()[indexPath.row]
            cell.updateViews(settingsAntiTheft: settingsAntiTheft)
            return cell
        } else {
            return SettingsAntiTheftCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let numPadVC = storyboard.instantiateViewController(withIdentifier: NUMBER_PAD) as? NumberPadVC {
            numPadVC.typeNumberPad = .repeatNewPassword
            self.navigationController?.pushViewController(numPadVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: MoreCellView! = MoreCellView()
        let headerHeight: CGFloat = 35
        return headerView.headerSettingsAntiTheftCellView(section: section, headerHeight: headerHeight)
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
