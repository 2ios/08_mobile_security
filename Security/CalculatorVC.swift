//
//  ViewController.swift
//  Calculator
//
//  Created by Mihail on 9/28/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import AVFoundation

class CalculatorVC: UIViewController {

    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    
    var player: AVAudioPlayer?
    
    var userIsTypingTheSecondDigit = false
    var hasTheNumberGotTheDot = false
    let numberOfDigitToDisplay = 9
    var currentNumberOfDigit = 1
    var password = ""
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkFirstTime()
    }
    
    func checkFirstTime() {
        if UserDefaults.standard.object(forKey: "firstTime") == nil {
            passwordLbl.text = "CREATE YOUR 4-DIGIT PIN AND TAP ±"
        }
    }
    
    
    @IBAction func touchDigit(_ sender: UIButton) {
        password += "\(sender.tag)"
        let digit = sender.currentTitle!
        if userIsTypingTheSecondDigit {
            let textCurrentlyInDisplay = display.text!
            if textCurrentlyInDisplay == "0" {
                display.text = digit
            } else {
                if currentNumberOfDigit < numberOfDigitToDisplay {
                    currentNumberOfDigit += 1
                } else {
                    return
                }
                display.text = textCurrentlyInDisplay + digit
            }
        } else {
            display.text = digit
            userIsTypingTheSecondDigit = true
        }
    }
    
    @IBAction func playSound(_ sender: UIButton) {
        if let sound = Bundle.main.url(forResource: "sound", withExtension: "wav") {
            do {
                player = try AVAudioPlayer(contentsOf: sound)
                guard let player = player else { return }
                player.volume = 0.05
                player.prepareToPlay()
                player.play()
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func dotDigit(_ sender: UIButton) {
        if !hasTheNumberGotTheDot {
            display.text = !userIsTypingTheSecondDigit ? "0" + sender.currentTitle! : display.text! + sender.currentTitle!
            hasTheNumberGotTheDot = true
            userIsTypingTheSecondDigit = true
        }
    }
    
    
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            if newValue.truncatingRemainder(dividingBy: 1.0) == 0 {
                let s = String(newValue)
                display.text = brain.reduce(String(s[s.startIndex...s.index(before: s.index(of: ".")!)]))
            } else {
                display.text = brain.reduce(String(newValue))
            }
        }
    }
    
    private var brain = CalculatorBrain()
    
    @IBAction func performOperation(_ sender: UIButton) {
        if sender.tag == 10 {
            if password.characters.count == 4 {
                if UserDefaults.standard.object(forKey: "firstTime") == nil {
                    UserDefaults.standard.set(true, forKey: "firstTime")
                    savePassword()
                } else {
                    checkPassword()
                }
            } else {
                password = ""
            }
        }
        if userIsTypingTheSecondDigit {
            brain.setOperand(displayValue)
            userIsTypingTheSecondDigit = false
            hasTheNumberGotTheDot = false
            currentNumberOfDigit = 1
        }
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(mathematicalSymbol)
        }
        if let result = brain.result {
                displayValue = result
        }
    }
    
    func savePassword() {
        UserDefaults.standard.set(password, forKey: "password")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let mainVC = storyboard.instantiateViewController(withIdentifier: "MainVC") as? MainVC {
            let navController = UINavigationController.init(rootViewController: mainVC)
            self.present(navController, animated: false, completion: nil)
        }
    }
    
    func checkPassword() {
        if let pass = UserDefaults.standard.object(forKey: "password") as? String {
            if password == pass {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let mainVC = storyboard.instantiateViewController(withIdentifier: "MainVC") as? MainVC {
                    let navController = UINavigationController.init(rootViewController: mainVC)
                    self.present(navController, animated: false, completion: nil)
                }
            } else {
                password = ""
            }
        }
    }
}



