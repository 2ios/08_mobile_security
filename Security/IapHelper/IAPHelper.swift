//
//  IAPHelper.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 19.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import StoreKit
import SwiftyStoreKit

let bundleID = "hoocheengre"

let sharedKey = "09b8504c380f49698b6c755061de768d"

enum RegisteredPurchase: String {
    case autoRenewablePurchase = "avtopodCheng"
}

class IAPHelper: NSObject {
    
    static let instance = IAPHelper()
    
    func initSwiftyStoreKit() {
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                if purchase.transaction.transactionState == .purchased || purchase.transaction.transactionState == .restored {
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("purchased: \(purchase)")
                }
            }
        }
    }
    
    func purchase(_ purchase: RegisteredPurchase, vc: UIViewController, completion: @escaping (_ result: Bool) -> ()) {
        AlertSpinner.instance.showSpinner(vc: vc)
        SwiftyStoreKit.purchaseProduct(purchase.rawValue, atomically: true) { result in
            if case .success(let purchaseResult) = result {
                // Deliver content from server, then:
                if purchaseResult.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchaseResult.transaction)
                }
                self.verifyProducts(purchase)
            AlertSpinner.instance.removeSpinner(vc: vc, completion: {
                    if let alert = self.alertForPurchaseResult(result) {
                        vc.showAlert(alert, completion: {
                            completion(true)
                        })
                    }
                })
            } else {
                AlertSpinner.instance.removeSpinner(vc: vc, completion: {
                    if let alert = self.alertForPurchaseResult(result) {
                        vc.showAlert(alert, completion: {
                            completion(false)
                        })
                    }
                })
            }
        }
    }
    
    func restorePurchases(vc: UIViewController, completion: @escaping (_ result: Bool) -> ()) {
        AlertSpinner.instance.showSpinner(vc: vc)
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            if results.restoreFailedPurchases.count > 0 {
                AlertSpinner.instance.removeSpinner(vc: vc, completion: {
                    vc.showAlert(self.alertForRestorePurchases(results), completion: {
                        completion(false)
                    })
                })
            } else if results.restoredPurchases.count > 0 {
                for purchase in results.restoredPurchases {
                    // fetch content from your server, then:
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                }
                self.verifyProducts(.autoRenewablePurchase)
                AlertSpinner.instance.removeSpinner(vc: vc, completion: {
                    vc.showAlert(self.alertForRestorePurchases(results), completion: {
                        completion(true)
                    })
                })
            } else {
                AlertSpinner.instance.removeSpinner(vc: vc, completion: {
                    vc.showAlert(self.alertForRestorePurchases(results), completion: {
                        completion(false)
                    })
                })
            }
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production)
        let password = sharedKey
        SwiftyStoreKit.verifyReceipt(using: appleValidator, password: password, completion: completion)
    }
    
    func verifyProducts(_ purchase: RegisteredPurchase) {
        verifyReceipt { (result) in
            switch result {
            case .success(let receipt):
                let productId = purchase.rawValue
                switch purchase {
                case .autoRenewablePurchase:
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        type: .autoRenewable,
                        productId: productId,
                        inReceipt: receipt,
                        validUntil: Date()
                    )
                    switch purchaseResult {
                    case .purchased(let expiryDate):
                        print("Product is valid until \(expiryDate.expiryDate)")
                        self.saveActiveSubscription(date: expiryDate.expiryDate)
                    case .expired(let expiryDate):
                        print("Product is expired since \(expiryDate.expiryDate)")
                    case .notPurchased:
                        print("This product has never been purchased")
                    }
                }
            case .error:
                print("Error to verifyProducts")
            }
        }
    }

    func saveActiveSubscription(date: Date) {
        if date.timeIntervalSince1970 > Date().timeIntervalSince1970 {
            UserDefaults.standard.set(date, forKey: "activeSubscription")
            UserDefaults.standard.synchronize()
            print("Salvat activeSubscription pina pe \(date)")
        }
    }
    
    var isSubscribed: Bool {
        guard let activeSubscription = UserDefaults.standard.object(forKey: "activeSubscription") as? Date else {
            return false
        }
        return activeSubscription.timeIntervalSince1970 > Date().timeIntervalSince1970
    }
}

extension UIViewController {
    func showAlert(_ alert: UIAlertController, completion: @escaping () -> ()) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            completion()
            return
        }
    }
}

extension IAPHelper {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    // swiftlint:disable cyclomatic_complexity
    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
        switch result {
        case .success(let purchase):
            print("Purchase Success: \(purchase.productId)")
            return alertWithTitle("Thank You", message: "Purchase completed")
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                return nil
            case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle("Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
            }
        }
    }
    
    func alertForRestorePurchases(_ results: RestoreResults) -> UIAlertController {
        if results.restoreFailedPurchases.count > 0 {
            print("Restore Failed: \(results.restoreFailedPurchases)")
            return alertWithTitle("Restore failed", message: "Unknown error. Please contact support")
        } else if results.restoredPurchases.count > 0 {
            print("Restore Success: \(results.restoredPurchases)")
            return alertWithTitle("Purchases Restored", message: "All purchases have been restored")
        } else {
            print("Nothing to Restore")
            return alertWithTitle("Nothing to restore", message: "No previous purchases were found")
        }
    }
}
