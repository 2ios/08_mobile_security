//
//  AlertSpinner.swift
//  Wi_Fi_passwords_from_airports
//
//  Created by User543 on 19.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AlertSpinner: UIAlertController {
    
    static let instance = AlertSpinner()
    
    var alert: UIAlertController?
    
    func showSpinner(vc: UIViewController) {
        alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.center = CGPoint(x: 135.0, y: 65.5)
        spinner.color = UIColor.black
        spinner.startAnimating()
        alert?.view.addSubview(spinner)
        vc.present(alert!, animated: true, completion: nil)
    }
    
    func removeSpinner(vc: UIViewController, completion: @escaping () -> ()) {
        alert?.dismiss(animated: true, completion: {
            completion()
        })
    }
}
