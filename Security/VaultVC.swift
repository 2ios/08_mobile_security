//
//  VaultVC.swift
//  Security
//
//  Created by User543 on 05.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class VaultVC: UIViewController, UITableViewDataSource, UITableViewDelegate, NumberPadVCDelegate {
    
    //Variables
    var numberPadVC: NumberPadVC?
    
    //Outlets
    @IBOutlet weak var categoryTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTable.tableFooterView = UIView()
        showNumberPad()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        categoryTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getCategoriesVault().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "VaultCell") as? VaultCell {
            let category = DataService.instance.getCategoriesVault()[indexPath.row]
            cell.updateViews(category: category, index: indexPath.row)
            return cell
        } else {
            return VaultCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let identifier = DataService.instance.getSeguesVault()[indexPath.row]
        performSegue(withIdentifier: identifier, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height / 7
    }

    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showNumberPad() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let numPadVC = storyboard.instantiateViewController(withIdentifier: NUMBER_PAD) as? NumberPadVC {
            numberPadVC = numPadVC
            numberPadVC?.delegate = self
            self.addChildVC(childVC: numberPadVC!)
        }
    }
    
   func removeNumberPad() -> Void {
        if let numPadVC = numberPadVC {
            self.removeChildVC(childVC: numPadVC)
            numberPadVC = nil
        }
    }
}
