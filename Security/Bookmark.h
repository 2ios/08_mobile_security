//
//  Bookmark.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bookmark : NSObject

@property (strong) NSString *name;
@property (strong) NSURL *url;

- (NSString *)urlString;
- (void)setUrlString:(NSString *)urls;

+ (void)retrieveList;
+ (void)persistList;
+ (NSMutableArray *)list;
+ (void)addBookmarkForURLString:(NSString *)urls withName:(NSString *)name;
+ (BOOL)isURLBookmarked:(NSURL *)url;
+ (UIAlertController *)addBookmarkDialogWithOkCallback:(void (^)(void))callback;

@end

