//
//  NumberPad.swift
//  Security
//
//  Created by User543 on 13.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

struct NumberPad {
    private(set) public var title: String
    
    init(title: String) {
        self.title = title
    }
}
