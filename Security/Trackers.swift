//
//  Trackers.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

struct Trackers {
    
    private(set) public var title: String
    private(set) public var date: String
    
    init (title: String, date: String) {
        self.title = title
        self.date = date
    }
}
