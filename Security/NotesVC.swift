//
//  NotesVC.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView
import AudioToolbox

class NotesVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LGAlertViewDelegate, UITextFieldDelegate {
    
    //Variables
    var notes = [Note]()
    
    //Outlets
    @IBOutlet weak var notesTable: UITableView!
    @IBOutlet weak var noNotesView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notesTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notes = Note.mr_findAll() as! [Note]
        checkNoNotes()
        notesTable.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell") as? NotesCell {
            let note = notes[indexPath.row]
            cell.updateViews(note: note)
            return cell
        } else {
            return NotesCell()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            tableView.setEditing(false, animated: true)
            let alert = LGAlertView(title: "Are you sure?", message: "Do you really want to delete this note?", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.deleteCellFromTable(atIndex: indexPath.row)}
            self.showAlertWithTwoButtons(alert: alert)
        }
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            if self.notes[indexPath.row].notePasscode != "" {
                let alert = LGAlertView(textFieldsAndTitle: "Enter passcode", message: "Enter the note's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                    self.setupTextField(textField: textField)
                }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
                alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscodeEdit(atIndex: indexPath.row, textField: alert.textFieldsArray?.first as! UITextField)}
                self.showAlertWithTextField(alert: alert)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let addNoteVC = storyboard.instantiateViewController(withIdentifier: ADD_NOTE_VC) as? AddNoteVC {
                    addNoteVC.note = self.notes[indexPath.row]
                    addNoteVC.typeNote = TypeNote.editNote
                    let navController = UINavigationController.init(rootViewController: addNoteVC)
                    self.present(navController, animated: true, completion: nil)
                }
            }
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        editAction.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
        return [deleteAction, editAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if notes[indexPath.row].notePasscode != "" {
            let alert = LGAlertView(textFieldsAndTitle: "Enter passcode", message: "Enter the note's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscode(atIndex: indexPath.row, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let addNoteVC = storyboard.instantiateViewController(withIdentifier: ADD_NOTE_VC) as? AddNoteVC {
                addNoteVC.note = notes[indexPath.row]
                addNoteVC.typeNote = TypeNote.showNote
                self.navigationController?.pushViewController(addNoteVC, animated: true)
            }
        }
    }

    func showAlertWithTextField(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4045503438, blue: 0.9838915467, alpha: 1)
        alert.showAnimated()
    }
    
    func setupTextField(textField: UITextField) {
        textField.delegate = self
        textField.isSecureTextEntry = true
    }
    
    func checkPasscode(atIndex index: Int, textField: UITextField) {
        let note = notes[index]
        if textField.text == note.notePasscode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let addNoteVC = storyboard.instantiateViewController(withIdentifier: ADD_NOTE_VC) as? AddNoteVC {
                addNoteVC.note = notes[index]
                addNoteVC.typeNote = .showNote
                self.navigationController?.pushViewController(addNoteVC, animated: true)
            }
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the note's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscode(atIndex: index, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        }
    }
    
    func checkPasscodeEdit(atIndex index: Int, textField: UITextField) {
        let note = notes[index]
        if textField.text == note.notePasscode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let addNoteVC = storyboard.instantiateViewController(withIdentifier: ADD_NOTE_VC) as? AddNoteVC {
                addNoteVC.note = self.notes[index]
                addNoteVC.typeNote = TypeNote.editNote
                let navController = UINavigationController.init(rootViewController: addNoteVC)
                self.present(navController, animated: true, completion: nil)
            }
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the note's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscodeEdit(atIndex: index, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        }
    }
    
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func deleteCellFromTable(atIndex index: Int) {
        self.notes.remove(at: index).mr_deleteEntity()
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        notesTable.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        notesTable.reloadData()
        checkNoNotes()
    }
    
    func checkNoNotes() {
        if notes.count > 0 {
            noNotesView.isHidden = true
        } else {
            noNotesView.isHidden = false
        }
    }
    
    func showAlertWithTwoButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
}
