//
//  RuleEditorController.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RuleEditorRow.h"

@interface RuleEditorController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDelegate>

@property AppDelegate *appDelegate;
@property NSMutableArray<RuleEditorRow *> *sortedRuleRows;
@property NSMutableArray<RuleEditorRow *> *inUseRuleRows;

@property UISearchBar *searchBar;
@property NSMutableArray<RuleEditorRow *> *searchResult;

- (NSString *)ruleDisabledReason:(RuleEditorRow *)row;
- (void)disableRuleForRow:(RuleEditorRow *)row withReason:(NSString *)reason;
- (void)enableRuleForRow:(RuleEditorRow *)row;

@end
