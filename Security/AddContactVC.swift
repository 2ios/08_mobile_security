//
//  AddContactVC.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView
import QBImagePickerController

class AddContactVC: UIViewController, LGAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, QBImagePickerControllerDelegate, UITextFieldDelegate {
    
    //Variables
    var contact: Contact!
    var typeContact = TypeContact.addContact
    var uiImagePicker: UIImagePickerController?
    var qbImagePicker: QBImagePickerController?
    var isAddContactAppear = true
    
    //Outlets
    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var contactFirstNameTxt: UITextField!
    @IBOutlet weak var contactSecondNameTxt: UITextField!
    @IBOutlet weak var contactCompanyTxt: UITextField!
    @IBOutlet weak var contactMobileTxt: UITextField!
    @IBOutlet weak var contactHomeTxt: UITextField!
    @IBOutlet weak var contactWorkTxt: UITextField!
    @IBOutlet weak var contactEmailTxt: UITextField!
    @IBOutlet weak var contactWebsiteTxt: UITextField!
    @IBOutlet weak var contactNoticeTxt: UITextField!
    @IBOutlet weak var passcodeView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(typeContact: typeContact)
        setupPlaceholder()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if typeContact == .addContact {
            if isAddContactAppear {
                contact = Contact.mr_createEntity()
                isAddContactAppear = false
            }
        } else {
            if isAddContactAppear {
                if let image = contact.contactImage as? UIImage {
                    contactImageView.image = image
                }
                contactFirstNameTxt.text = contact.contactFirstName
                contactSecondNameTxt.text = contact.contactSecondName
                contactCompanyTxt.text = contact.contactCompany
                contactMobileTxt.text = contact.contactMobile
                contactHomeTxt.text = contact.contactHome
                contactWorkTxt.text = contact.contactWork
                contactEmailTxt.text = contact.contactEmail
                contactWebsiteTxt.text = contact.contactWebsite
                contactNoticeTxt.text = contact.contactNotice
                
                if typeContact == .showContact {
                    contactFirstNameTxt.isUserInteractionEnabled = false
                    contactSecondNameTxt.isUserInteractionEnabled = false
                    contactCompanyTxt.isUserInteractionEnabled = false
                    contactMobileTxt.isUserInteractionEnabled = false
                    contactHomeTxt.isUserInteractionEnabled = false
                    contactWorkTxt.isUserInteractionEnabled = false
                    contactEmailTxt.isUserInteractionEnabled = false
                    contactWebsiteTxt.isUserInteractionEnabled = false
                    contactNoticeTxt.isUserInteractionEnabled = false
                    passcodeView.isHidden = true
                }
                navigationItem.title = contact.contactFirstName
                isAddContactAppear = false
            }
        }
    }

    @IBAction func navBackBtnPressed(_ sender: Any) {
        if typeContact == .addContact {
            contact.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        if checkSubscription() {
            if contact.contactPasscode == nil {
                contact.contactPasscode = ""
            }
            contact.contactImage = contactImageView.image
            contact.contactFirstName = contactFirstNameTxt.text
            contact.contactSecondName = contactSecondNameTxt.text
            contact.contactCompany = contactCompanyTxt.text
            contact.contactMobile = contactMobileTxt.text
            contact.contactHome = contactHomeTxt.text
            contact.contactWork = contactWorkTxt.text
            contact.contactEmail = contactEmailTxt.text
            contact.contactWebsite = contactWebsiteTxt.text
            contact.contactNotice = contactNoticeTxt.text
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setNavigationBar(typeContact: TypeContact) {
        switch typeContact {
        case .editContact:
            editContactNavigation()
        case .showContact:
            showContactNavigation()
        case .addContact:
            addContactNavigation()
        }
    }
    
    func editContactNavigation() {
        navigationItem.title = "EDIT CONTACT"
        setupNavigationBar()
    }
    
    func addContactNavigation() {
        navigationItem.title = "ADD CONTACT"
        setupNavigationBar()
    }
    
    func showContactNavigation() {
        if contact.contactFirstName == nil {
            navigationItem.title = "No firstname"
        } else {
            navigationItem.title = contact.contactFirstName
        }
        setupNavigationBar()
        navigationItem.rightBarButtonItem = UIBarButtonItem()
        let leftBtn = UIButton(type: .custom)
        leftBtn.addTarget(self, action: #selector(backButton), for: .touchUpInside)
        if let image = UIImage(named: "nav_back_button") {
            leftBtn.setImage(image, for: .normal)
            leftBtn.frame.size = CGSize(width: image.size.width, height: image.size.height)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBtn)
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2039215686, green: 0.3843137255, blue: 0.5176470588, alpha: 1)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func backButton() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addPhotoBtnPressed(_ sender: Any) {
        if typeContact != .showContact {
            let alert = LGAlertView(title: nil, message: nil, style: LGAlertViewStyle.actionSheet, buttonTitles: ["Photo Library", "Camera"], cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, delegate: self)
            self.showAlertWithThreeButtons(alert: alert)
        }
    }
    
    func showAlertWithThreeButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
    
    func alertView(_ alertView: LGAlertView, clickedButtonAt index: UInt, title: String?) {
        if index == 0 {
            takePhotoFromLibrary()
        } else {
            takePhotoFromCamera()
        }
    }
    
    func takePhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            qbImagePicker = QBImagePickerController()
            qbImagePicker?.delegate = self
            qbImagePicker?.mediaType = .image
            qbImagePicker?.allowsMultipleSelection = true
            qbImagePicker?.maximumNumberOfSelection = 1
            qbImagePicker?.showsNumberOfSelectedAssets = true
            self.present(qbImagePicker!, animated: true, completion: nil)
        }
    }

    func takePhotoFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            uiImagePicker = UIImagePickerController()
            uiImagePicker?.delegate = self
            uiImagePicker?.sourceType  = .camera
            uiImagePicker?.allowsEditing = false
            self.present(uiImagePicker!, animated: true, completion: nil)
        }
    }

    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        let phAssets = assets as! [PHAsset]
        for i in 0..<phAssets.count {
            autoreleasepool {
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                option.isSynchronous = true
                manager.requestImage(for: phAssets[i], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFit, options: option, resultHandler: {(thumbImage, info)->Void in
                    if let thumbImage = thumbImage {
                        self.contactImageView.image = thumbImage
                    }
                })
            }
        }
        imagePickerController.dismiss(animated: true, completion: nil)
        qbImagePicker = nil
    }

    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        imagePickerController.dismiss(animated: true, completion: nil)
        qbImagePicker = nil
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        picker.dismiss(animated: true, completion: nil)
        uiImagePicker = nil
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if error == nil {
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            let fetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
            if let phAsset = fetchResult.lastObject {
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                option.isSynchronous = true
                manager.requestImage(for: phAsset, targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFit, options: option, resultHandler: {(thumbImage, info)->Void in
                    if let thumbImage = thumbImage {
                        self.contactImageView.image = thumbImage
                    }
                })
            }
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        uiImagePicker = nil
    }
    
    func setupPlaceholder() {
        contactFirstNameTxt.attributedPlaceholder = NSAttributedString(string: "First name", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactSecondNameTxt.attributedPlaceholder = NSAttributedString(string: "Second name", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactCompanyTxt.attributedPlaceholder = NSAttributedString(string: "Company", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactMobileTxt.attributedPlaceholder = NSAttributedString(string: "Mobile", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactHomeTxt.attributedPlaceholder = NSAttributedString(string: "Home", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactWorkTxt.attributedPlaceholder = NSAttributedString(string: "Work", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactEmailTxt.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactWebsiteTxt.attributedPlaceholder = NSAttributedString(string: "Website", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        contactNoticeTxt.attributedPlaceholder = NSAttributedString(string: "Notice", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
    }
    
    func showAlertFreeTrial() {
        let alert = LGAlertView(title: "Start Your Free Trial", message: "You can instantly enjoy free Premium Security to create unlimited contacts at any time", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "NO, THANKS", destructiveButtonTitle: "CONTINUE", delegate: self)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.buySubscription()}
        alert.didDismissAfterCancelHandler = { [unowned self](alertView: LGAlertView) in self.cancelSubscription()}
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func checkSubscription() -> Bool {
        if !IAPHelper.instance.isSubscribed, Contact.mr_findAll()!.count > 5 {
            showAlertFreeTrial()
            return false
        }
        return true
    }
    
    func buySubscription() {
        IAPHelper.instance.purchase(.autoRenewablePurchase, vc: self) { (success) in
            if success {
                print("Success buy subscription")
            } else {
                print("No success buy subscription")
            }
        }
    }
    
    func cancelSubscription() {
        if typeContact == .addContact {
            contact.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func passcodeBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let addContactPasscodeVC = storyboard.instantiateViewController(withIdentifier: ADD_CONTACT_PASSCODE_VC) as? AddContactPasscodeVC {
            addContactPasscodeVC.contact = contact
            addContactPasscodeVC.typeContact = typeContact
            self.navigationController?.pushViewController(addContactPasscodeVC, animated: true)
        }
    }
    
    // MARK: Keyboard control
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}


