//
//  NSString+JavascriptEscape.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JavaScriptEscape)

- (NSString *)stringEscapedForJavasacript;

@end

