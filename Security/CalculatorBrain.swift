//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Mihail on 9/28/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import Foundation

struct CalculatorBrain {
    
    private var accumulator: Double?
    private var isFirstOperandSet = false
    private var prevAccumulator: Double?
    
    private enum Operation {
        case constant(Double)
        case unaryOperation((Double) -> Double)
        case binaryOperation((Double, Double) -> Double)
        case equals
    }
    private var operations: Dictionary<String, Operation> =
        [
            "π" : Operation.constant(Double.pi),
            "e" : Operation.constant(M_E),
            "√" : Operation.unaryOperation(sqrt),
            "cos" : Operation.unaryOperation(cos),
            "±" : Operation.unaryOperation({ -$0 }),
            "×" : Operation.binaryOperation({$0 * $1}),
            "÷" : Operation.binaryOperation({$0 / $1}),
            "+" : Operation.binaryOperation({$0 + $1}),
            "-" : Operation.binaryOperation({$0 - $1}),
            "=" : Operation.equals,
            "c" : Operation.constant(0)
        ]
    mutating func performOperation(_ symbol: String) {
        if let operation = operations[symbol] {
            switch operation {
            case .constant(let value):
                accumulator = value
                if value == 0 {
                    isFirstOperandSet = false
                }
            case .unaryOperation(let function):
                if accumulator != nil {
                    accumulator = function(accumulator!)
                }
            case .binaryOperation(let function):
                if accumulator != nil {
                    if isFirstOperandSet {
                        prevAccumulator = nil
                        performPendingBinaryOperation()
                    }
                    pendingbinaryoperation = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                    isFirstOperandSet = true
                }
            case .equals:
                prevAccumulator = accumulator
                performPendingBinaryOperation()
            }
        }
    }
    
    private mutating func performPendingBinaryOperation() {
        if pendingbinaryoperation != nil && accumulator != nil {
            accumulator = pendingbinaryoperation!.perform(with: prevAccumulator != nil ? prevAccumulator! : accumulator!)
        }
    }
    
    private var pendingbinaryoperation: PendingBinaryOperation?
    private struct PendingBinaryOperation {
        let function: (Double, Double) -> Double
        var firstOperand: Double
        
        func perform(with secondOperand: Double) -> Double {
            return function(firstOperand, secondOperand)
        }
    }
    
    mutating func setOperand(_ operand: Double) {
        accumulator = operand
    }
    
    var result: Double? {
        get {
            return accumulator
        }
    }
    
    mutating func reduce(_ number: String) -> String {
        if (number.characters.count < 10) {
            return number
        } else {
            let endIndex = number.index(number.startIndex, offsetBy: 8)
            var exponent: Int
            if let dotIndex = number.index(of: ".") {
                if dotIndex < endIndex {
                    return String(number[number.startIndex...number.index(after: endIndex)])
                } else {
                    exponent = number.distance(from: endIndex, to: dotIndex)
                }
            } else {
                exponent = number.distance(from: endIndex, to: number.endIndex)
            }
            exponent += String(exponent).characters.count + 1
            let strExponent = String(exponent)
            return String(number[number.startIndex...number.index(number.startIndex, offsetBy: 7 - strExponent.characters.count)] + "e" + strExponent)
        }
    }
}
