//
//  BorderRoundedButtonView.swift
//  Security
//
//  Created by User543 on 18.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class BorderRoundedButtonView: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: CGColor = #colorLiteral(red: 0.4258278012, green: 0.5784519911, blue: 0.703417182, alpha: 1) {
        didSet {
            self.layer.borderColor = borderColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
    }
}
