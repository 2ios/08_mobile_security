//
//  PhotosVC.swift
//  Security
//
//  Created by User543 on 21.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import Photos
import LGAlertView
import CoreData
import QBImagePickerController
import MagicalRecord
import DTPhotoViewerController


class PhotosVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, LGAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, QBImagePickerControllerDelegate, UICollectionViewDelegateFlowLayout, DTPhotoViewerControllerDataSource, DTPhotoViewerControllerDelegate {

    //Variables
    var album: Album!
    var typeCollectionImages = TypeCollectionImages.addImages
    var imageArrayStr = [String]()
    var uiImagePicker: UIImagePickerController?
    var qbImagePicker: QBImagePickerController?
    var navigation: NavigationNumberPad!
    var isSelectedMode = false
    var selectedImageIndex: Int = 0
    
    //Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noPhotosView: UIView!
    @IBOutlet weak var trashBarBtn: UIBarButtonItem!
    @IBOutlet weak var selectedBarBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let array = album.albumImagesStr {
            imageArrayStr = array as! [String]
        }
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
        navigationItem.title = album.albumName
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkNoPhotos()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.presentedViewController == nil {
            album.albumImagesStr = self.imageArrayStr as NSObject
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(imageArrayStr.count)
        return imageArrayStr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as? PhotoCell {
            cell.updateViews(image: load(fileName: imageArrayStr[indexPath.item]))
            cell.isSelectedImageView.isHidden = true
            return cell
        } else {
            return PhotoCell()
        }
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addPhotoBtnPressed(_ sender: Any) {
        let alert = LGAlertView(title: nil, message: nil, style: LGAlertViewStyle.actionSheet, buttonTitles: ["Photo Library", "Camera"], cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, delegate: self)
        self.showAlertWithThreeButtons(alert: alert)
    }
    
    func showAlertWithThreeButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
    
    func alertView(_ alertView: LGAlertView, clickedButtonAt index: UInt, title: String?) {
        if index == 0 {
            takePhotoFromLibrary()
        } else {
            takePhotoFromCamera()
        }
    }

    func takePhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            qbImagePicker = QBImagePickerController()
            qbImagePicker?.delegate = self
            qbImagePicker?.mediaType = .image
            qbImagePicker?.allowsMultipleSelection = true
            qbImagePicker?.maximumNumberOfSelection = 50
            qbImagePicker?.showsNumberOfSelectedAssets = true
            self.present(qbImagePicker!, animated: true, completion: nil)
        }
    }
    
    func takePhotoFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            uiImagePicker = UIImagePickerController()
            uiImagePicker?.delegate = self
            uiImagePicker?.sourceType  = .camera
            uiImagePicker?.allowsEditing = false
            self.present(uiImagePicker!, animated: true, completion: nil)
        }
    }

    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        let phAssets = assets as! [PHAsset]
        var count = 0
        if checkSubscription(photos: imageArrayStr.count + phAssets.count) {
            for i in 0..<phAssets.count {
                autoreleasepool {
                    phAssets[i].getURL { (url) in
                        if let url = url {
                            let path = self.album.pathAlbum
                            let file = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(path!)
                            let imageName = NSUUID().uuidString + ".jpg"
                            let pathImage = file.appendingPathComponent(imageName)
                            do {
                                try FileManager.default.copyItem(atPath: url.path, toPath: pathImage.path)
                                self.imageArrayStr.append(imageName)
                                count += 1
                                if count == phAssets.count{
                                    self.collectionView.reloadData()
                                    self.checkNoPhotos()
                                }
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    }
                }
            }
        }
        imagePickerController.dismiss(animated: true, completion: nil)
        qbImagePicker = nil
    }

    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        imagePickerController.dismiss(animated: true, completion: nil)
        qbImagePicker = nil
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if checkSubscription(photos: imageArrayStr.count + 1) {
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
        picker.dismiss(animated: true, completion: nil)
        uiImagePicker = nil
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if error == nil {
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            let fetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
            if let phAsset = fetchResult.lastObject {
                phAsset.getURL { (url) in
                    if let url = url {
                        let path = self.album.pathAlbum
                        let file = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(path!)
                        let imageName = NSUUID().uuidString + ".jpg"
                        let pathImage = file.appendingPathComponent(imageName)
                        do {
                            try FileManager.default.copyItem(atPath: url.path, toPath: pathImage.path)
                            self.imageArrayStr.append(imageName)
                            self.collectionView.reloadData()
                            self.checkNoPhotos()
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        uiImagePicker = nil
    }
    
    func checkNoPhotos() {
        if imageArrayStr.count > 0 {
            noPhotosView.isHidden = true
        } else {
            noPhotosView.isHidden = false
        }
    }
    
    @IBAction func navSelectBtnPressed(_ sender: Any) {
        if !isSelectedMode {
            collectionView.allowsMultipleSelection = true
            isSelectedMode = true
            selectedBarBtn.tintColor = #colorLiteral(red: 0.09731664509, green: 0.2519248724, blue: 0.3972762227, alpha: 1)
        } else {
            collectionView.allowsMultipleSelection = false
            isSelectedMode = false
            selectedBarBtn.tintColor = #colorLiteral(red: 0.5607843137, green: 0.6156862745, blue: 0.6666666667, alpha: 1)
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !isSelectedMode {
            selectedImageIndex = indexPath.item
            if let cell = collectionView.cellForItem(at: indexPath) as? PhotoCell {
                if let viewController = DTPhotoViewerController(referencedView: cell.imageView, image: cell.imageView.image) {
                    viewController.dataSource = self
                    viewController.delegate = self
                    self.present(viewController, animated: true, completion: nil)
                }
            }
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as? PhotoCell {
                cell.isSelectedImageView.isHidden = false
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if isSelectedMode {
            if let cell = collectionView.cellForItem(at: indexPath) as? PhotoCell {
                cell.isSelectedImageView.isHidden = true
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var n = 0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            n = 6
        } else {
            n = 3
        }
        let width = self.collectionView.frame.width - CGFloat((n-1)*10)
        let widthCell = width / CGFloat(n)
        let heightCell = widthCell
        return CGSize(width: widthCell, height: heightCell)
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        // Set text for each item
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = self.collectionView?.cellForItem(at: indexPath) as? PhotoCell {
            return cell.imageView
        }
        return nil
    }
    
    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return imageArrayStr.count
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        imageView.image = load(fileName: imageArrayStr[index])
    }
    
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
    }
    
    var documentsUrl: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    private func load(fileName: String) -> UIImage? {
        let fileURL = documentsUrl.appendingPathComponent(self.album.pathAlbum!).appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }
    
    @IBAction func navTrashBtnPressed(_ sender: Any) {
        if isSelectedMode {
            showAlertRemovePhotos()
        } else {
            showAlertForDelete()
        }
    }
    
    func showAlertRemovePhotos() {
        let alert = LGAlertView(title: "Are you sure?", message: "Do you want to delete \(self.collectionView.indexPathsForSelectedItems!.count) photos?", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "NO, THANKS", destructiveButtonTitle: "CONTINUE", delegate: self)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.removePhotos()}
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func showAlertDoneRemove() {
        let alert = LGAlertView(title: nil, message: "Done", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: nil, destructiveButtonTitle: "OK", delegate: self)
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func showAlertForDelete() {
        let alert = LGAlertView(title: "", message: "You need select image(s) before delete", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: nil, destructiveButtonTitle: "OK", delegate: self)
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func removePhotos() {
        var imagesToDelete = [String]()
        for index in self.collectionView.indexPathsForSelectedItems! {
            imagesToDelete.append(imageArrayStr[index.item])
        }
        for imageName in imagesToDelete {
            let pathToAlbum = album.pathAlbum!
            let FileToDelete = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(pathToAlbum).appendingPathComponent(imageName)
            let fileManager = FileManager.default
            
            do {
                try fileManager.removeItem(atPath: FileToDelete.path)
                if let index = imageArrayStr.index(of: imageName) {
                    imageArrayStr.remove(at: index)
                }
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
        }
        self.collectionView.reloadData()
        isSelectedMode = false
        selectedBarBtn.tintColor = #colorLiteral(red: 0.5607843137, green: 0.6156862745, blue: 0.6666666667, alpha: 1)
        self.checkNoPhotos()
        showAlertDoneRemove()
    }
    
    func showAlertFreeTrial() {
        let alert = LGAlertView(title: "Start Your Free Trial", message: "You can instantly enjoy free Premium Security to add unlimited photos at any time", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "NO, THANKS", destructiveButtonTitle: "CONTINUE", delegate: self)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.buySubscription()}
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func checkSubscription(photos: Int) -> Bool {
        if !IAPHelper.instance.isSubscribed, photos > 5 {
            showAlertFreeTrial()
            return false
        }
        return true
    }
    
    func buySubscription() {
        IAPHelper.instance.purchase(.autoRenewablePurchase, vc: self) { (success) in
            if success {
                print("Success buy subscription")
            } else {
                print("No success buy subscription")
            }
        }
    }
}
