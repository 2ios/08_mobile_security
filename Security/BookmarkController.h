//
//  BookmarkController.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookmarkController : UITableViewController <UIGestureRecognizerDelegate>

@property BOOL embedded;

@end
