//
//  Constants.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

// Segues
let TO_VAULT = "toVault"

let TO_ALBUMS = "toAlbums"
let TO_NOTES = "toNotes"
let TO_CONTACTS = "toContacts"
let TO_PASSWORDS = "toPasswords"

let TO_MALWARE = "toMalware"
let TO_TRACKERS = "toTrackers"
let TO_ADVERTISEMENTS = "toAdvertisements"

// StoryboardIDs
let NUMBER_PAD = "NumberPad"
let VAULT_VC = "VaultVC"
let REPORTS_VC = "ReportsVC"
let ADD_ALBUM_VC  = "AddAlbumVC"
let ADD_ALBUM_PASSCODE_VC = "AddAlbumPasscodeVC"
let ADD_NOTE_PASSCODE_VC = "AddNotePasscodeVC"
let ADD_CONTACT_PASSCODE_VC = "AddContactPasscodeVC"
let ADD_PASSWORD_PASSCODE_VC = "AddPasswordPasscodeVC"
let ADD_NOTE_VC = "AddNoteVC"
let ADD_CONTACT_VC = "AddContactVC"
let ADD_PASSWORD_VC = "AddPasswordVC"
let PHOTO_VC = "PhotosVC"
let WEB_VC = "WebVC"
let CALCULATOR_VC = "CalculatorVC"

