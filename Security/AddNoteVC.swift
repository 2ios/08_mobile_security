//
//  AddNoteVC.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView

class AddNoteVC: UIViewController, LGAlertViewDelegate, UITextFieldDelegate, UITextViewDelegate {

    //Variables
    var note: Note!
    var typeNote = TypeNote.addNote
    var isAddNoteAppear = true
    
    //Outlets
    
    @IBOutlet weak var noteName: UITextField!
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var passcodeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(typeNote: typeNote)
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if typeNote == .addNote {
            if isAddNoteAppear {
                note = Note.mr_createEntity()
                isAddNoteAppear = false
            }
        } else {
            if isAddNoteAppear {
                noteName.text = note.noteName
                textField.text = note.noteText
                if typeNote == .showNote {
                    noteName.isUserInteractionEnabled = false
                    textField.isUserInteractionEnabled = false
                    passcodeView.isHidden = true
                }
                navigationItem.title = note.noteName
                isAddNoteAppear = false
            }
        }
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        if typeNote == .addNote {
            note.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        if checkSubscription() {
            if note.notePasscode == nil {
                note.notePasscode = ""
            }
            note.noteName = noteName.text
            note.noteText = textField.text
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setNavigationBar(typeNote: TypeNote) {
        switch typeNote {
        case .editNote:
            editNoteNavigation()
        case .showNote:
            showNoteNavigation()
        case .addNote:
            addNoteNavigation()
        }
    }
    
    func editNoteNavigation() {
        navigationItem.title = "EDIT NOTE"
        setupNavigationBar()
    }
    
    func addNoteNavigation() {
        navigationItem.title = "ADD NOTE"
        setupNavigationBar()
    }
    
    func showNoteNavigation() {
        if note.noteName == "" {
            navigationItem.title = "No title"
        } else {
            navigationItem.title = note.noteName
        }
        setupNavigationBar()
        navigationItem.rightBarButtonItem = UIBarButtonItem()
        let leftBtn = UIButton(type: .custom)
        leftBtn.addTarget(self, action: #selector(backButton), for: .touchUpInside)
        if let image = UIImage(named: "nav_back_button") {
            leftBtn.setImage(image, for: .normal)
            leftBtn.frame.size = CGSize(width: image.size.width, height: image.size.height)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBtn)
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2039215686, green: 0.3843137255, blue: 0.5176470588, alpha: 1)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func backButton() {
        navigationController?.popViewController(animated: true)
    }
    
    func showAlertFreeTrial() {
        let alert = LGAlertView(title: "Start Your Free Trial", message: "You can instantly enjoy free Premium Security to create unlimited notes at any time", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "NO, THANKS", destructiveButtonTitle: "CONTINUE", delegate: self)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.buySubscription()}
        alert.didDismissAfterCancelHandler = { [unowned self](alertView: LGAlertView) in self.cancelSubscription()}
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func checkSubscription() -> Bool {
        if !IAPHelper.instance.isSubscribed, Note.mr_findAll()!.count > 5 {
            showAlertFreeTrial()
            return false
        }
        return true
    }
    
    func buySubscription() {
        IAPHelper.instance.purchase(.autoRenewablePurchase, vc: self) { (success) in
            if success {
                print("Success buy subscription")
            } else {
                print("No success buy subscription")
            }
        }
    }
    
    func cancelSubscription() {
        if typeNote == .addNote {
            note.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func passcodeBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let addNotePasscodeVC = storyboard.instantiateViewController(withIdentifier: ADD_NOTE_PASSCODE_VC) as? AddNotePasscodeVC {
            addNotePasscodeVC.note = note
            addNotePasscodeVC.typeNote = typeNote
            self.navigationController?.pushViewController(addNotePasscodeVC, animated: true)
        }
    }
    
    // MARK: Keyboard control
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if typeNote == .addNote {
           textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if typeNote == .addNote {
            if textView.text == "" {
                textView.text = "Enter text ..."
            }
        }
    }
}

