//
//  AppDelegate.m
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import <MagicalRecord/MagicalRecord.h>
#import "Bookmark.h"
#import "HTTPSEverywhere.h"
#import "URLInterceptor.h"

#import "UIResponder+FirstResponder.h"
#import "Security-Swift.h"

@interface AppDelegate ()

@property (strong, nonatomic) CalculatorVC *calcVC;

@end

@implementation AppDelegate
{
    NSMutableArray *_keyCommands;
    NSMutableArray *_allKeyBindings;
    NSArray *_allCommandsAndKeyBindings;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeDefaults];
    
//    [NSURLProtocol registerClass:[URLInterceptor class]];
    
    self.hstsCache = [HSTSCache retrieve];
    self.cookieJar = [[CookieJar alloc] init];
    [Bookmark retrieveList];
    
    /* handle per-version upgrades or migrations */
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    long lastBuild = [userDefaults integerForKey:@"last_build"];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    long thisBuild = [[f numberFromString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]] longValue];
    
    if (lastBuild != thisBuild) {
        NSLog(@"migrating from build %ld -> %ld", lastBuild, thisBuild);
        [HostSettings migrateFromBuild:lastBuild toBuild:thisBuild];
        
        [userDefaults setInteger:thisBuild forKey:@"last_build"];
        [userDefaults synchronize];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Security"];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
    [[IAPHelper instance] initSwiftyStoreKit];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.calcVC = [storyboard instantiateViewControllerWithIdentifier:@"CalculatorVC"];
    self.window.rootViewController = self.calcVC;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [application ignoreSnapshotOnNextApplicationLaunch];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults boolForKey:@"clear_on_background"]) {
        [[self webViewController] removeAllTabs];
        [[self cookieJar] clearAllNonWhitelistedData];
    }
    else
        [[self cookieJar] clearAllOldNonWhitelistedData];
    
    [application ignoreSnapshotOnNextApplicationLaunch];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    NumberPadVC *numPadVC = [storyboard instantiateViewControllerWithIdentifier:@"NumberPad"];
//    if (numPadVC) {
//        numPadVC.typeNumberPad = TypeNumberPadForeground;
//        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:numPadVC];
//        [[[self window] rootViewController] presentViewController:navigation animated:NO completion:nil];
//    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    self.calcVC = [storyboard instantiateViewControllerWithIdentifier:@"CalculatorVC"];
    self.window.rootViewController = self.calcVC;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    [[self webViewController] viewIsVisible];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    /* this definitely ends our sessions */
    [[self cookieJar] clearAllNonWhitelistedData];
    
    [application ignoreSnapshotOnNextApplicationLaunch];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[[url scheme] lowercaseString] isEqualToString:@"endlesshttp"])
        url = [NSURL URLWithString:[[url absoluteString] stringByReplacingCharactersInRange:NSMakeRange(0, [@"endlesshttp" length]) withString:@"http"]];
    else if ([[[url scheme] lowercaseString] isEqualToString:@"endlesshttps"])
        url = [NSURL URLWithString:[[url absoluteString] stringByReplacingCharactersInRange:NSMakeRange(0, [@"endlesshttps" length]) withString:@"https"]];
    
    [[self webViewController] dismissViewControllerAnimated:YES completion:nil];
    [[self webViewController] addNewTabForURL:url];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier {
    if ([extensionPointIdentifier isEqualToString:UIApplicationKeyboardExtensionPointIdentifier]) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        return [userDefaults boolForKey:@"third_party_keyboards"];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{

    /* if we tried last time and failed, the state might be corrupt */
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:STATE_RESTORE_TRY_KEY] != nil) {
        NSLog(@"[AppDelegate] previous startup failed, not restoring application state");
        [userDefaults removeObjectForKey:STATE_RESTORE_TRY_KEY];
        return NO;
    }
    else
        [userDefaults setBool:YES forKey:STATE_RESTORE_TRY_KEY];
    
    [userDefaults synchronize];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:@"clear_on_background"])
        return NO;
    
    return YES;
}

- (NSArray<UIKeyCommand *> *)keyCommands
{
    if (!_keyCommands) {
        _keyCommands = [[NSMutableArray alloc] init];
        
        [_keyCommands addObject:[UIKeyCommand keyCommandWithInput:UIKeyInputLeftArrow modifierFlags:UIKeyModifierCommand action:@selector(handleKeyboardShortcut:) discoverabilityTitle:NSLocalizedString(@"Go Back", nil)]];
        [_keyCommands addObject:[UIKeyCommand keyCommandWithInput:UIKeyInputRightArrow modifierFlags:UIKeyModifierCommand action:@selector(handleKeyboardShortcut:) discoverabilityTitle:NSLocalizedString(@"Go Forward", nil)]];
        
        [_keyCommands addObject:[UIKeyCommand keyCommandWithInput:@"b" modifierFlags:UIKeyModifierCommand action:@selector(handleKeyboardShortcut:) discoverabilityTitle:NSLocalizedString(@"Show Bookmarks", nil)]];
        
        [_keyCommands addObject:[UIKeyCommand keyCommandWithInput:@"l" modifierFlags:UIKeyModifierCommand action:@selector(handleKeyboardShortcut:) discoverabilityTitle:NSLocalizedString(@"Focus URL Field", nil)]];
        
        [_keyCommands addObject:[UIKeyCommand keyCommandWithInput:@"t" modifierFlags:UIKeyModifierCommand action:@selector(handleKeyboardShortcut:) discoverabilityTitle:NSLocalizedString(@"Create New Tab", nil)]];
        [_keyCommands addObject:[UIKeyCommand keyCommandWithInput:@"w" modifierFlags:UIKeyModifierCommand action:@selector(handleKeyboardShortcut:) discoverabilityTitle:NSLocalizedString(@"Close Tab", nil)]];
        
        for (int i = 1; i <= 10; i++)
            [_keyCommands addObject:[UIKeyCommand keyCommandWithInput:[NSString stringWithFormat:@"%d", (i == 10 ? 0 : i)] modifierFlags:UIKeyModifierCommand action:@selector(handleKeyboardShortcut:) discoverabilityTitle:[NSString stringWithFormat:NSLocalizedString(@"Switch to Tab %d", nil), i]]];
    }
    
    if (!_allKeyBindings) {
        _allKeyBindings = [[NSMutableArray alloc] init];
        const long modPermutations[] = {
            UIKeyModifierAlphaShift,
            UIKeyModifierShift,
            UIKeyModifierControl,
            UIKeyModifierAlternate,
            UIKeyModifierCommand,
            UIKeyModifierCommand | UIKeyModifierAlternate,
            UIKeyModifierCommand | UIKeyModifierControl,
            UIKeyModifierControl | UIKeyModifierAlternate,
            UIKeyModifierControl | UIKeyModifierCommand,
            UIKeyModifierControl | UIKeyModifierAlternate | UIKeyModifierCommand,
            kNilOptions,
        };
        
        NSString *chars = @"`1234567890-=\b\tqwertyuiop[]\\asdfghjkl;'\rzxcvbnm,./ ";
        for (int j = 0; j < sizeof(modPermutations); j++) {
            for (int i = 0; i < [chars length]; i++) {
                NSString *c = [chars substringWithRange:NSMakeRange(i, 1)];
                
                [_allKeyBindings addObject:[UIKeyCommand keyCommandWithInput:c modifierFlags:modPermutations[j] action:@selector(handleKeyboardShortcut:)]];
            }
            
            [_allKeyBindings addObject:[UIKeyCommand keyCommandWithInput:UIKeyInputUpArrow modifierFlags:modPermutations[j] action:@selector(handleKeyboardShortcut:)]];
            [_allKeyBindings addObject:[UIKeyCommand keyCommandWithInput:UIKeyInputDownArrow modifierFlags:modPermutations[j] action:@selector(handleKeyboardShortcut:)]];
            [_allKeyBindings addObject:[UIKeyCommand keyCommandWithInput:UIKeyInputLeftArrow modifierFlags:modPermutations[j] action:@selector(handleKeyboardShortcut:)]];
            [_allKeyBindings addObject:[UIKeyCommand keyCommandWithInput:UIKeyInputRightArrow modifierFlags:modPermutations[j] action:@selector(handleKeyboardShortcut:)]];
            [_allKeyBindings addObject:[UIKeyCommand keyCommandWithInput:UIKeyInputEscape modifierFlags:modPermutations[j] action:@selector(handleKeyboardShortcut:)]];
        }
        
        _allCommandsAndKeyBindings = [_keyCommands arrayByAddingObjectsFromArray:_allKeyBindings];
    }
    
    /* if settings are up or something else, ignore shortcuts */
    if (![[self topViewController] isKindOfClass:[WebViewController class]])
        return nil;
    
    id cur = [UIResponder currentFirstResponder];
    if (cur == nil || [NSStringFromClass([cur class]) isEqualToString:@"UIWebView"])
        return _allCommandsAndKeyBindings;
    else {
        return _keyCommands;
    }
}

- (void)handleKeyboardShortcut:(UIKeyCommand *)keyCommand
{
    if ([keyCommand modifierFlags] == UIKeyModifierCommand) {
        if ([[keyCommand input] isEqualToString:@"b"]) {
            [[self webViewController] showBookmarksForEditing:NO];
            return;
        }
        
        if ([[keyCommand input] isEqualToString:@"l"]) {
            [[self webViewController] focusUrlField];
            return;
        }
        
        if ([[keyCommand input] isEqualToString:@"t"]) {
            [[self webViewController] addNewTabForURL:nil forRestoration:NO withCompletionBlock:^(BOOL finished) {
                [[self webViewController] focusUrlField];
            }];
            return;
        }
        
        if ([[keyCommand input] isEqualToString:@"w"]) {
            [[self webViewController] removeTab:[[[self webViewController] curWebViewTab] tabIndex]];
            return;
        }
        
        if ([[keyCommand input] isEqualToString:UIKeyInputLeftArrow]) {
            [[[self webViewController] curWebViewTab] goBack];
            return;
        }
        
        if ([[keyCommand input] isEqualToString:UIKeyInputRightArrow]) {
            [[[self webViewController] curWebViewTab] goForward];
            return;
        }
        
        for (int i = 0; i <= 9; i++) {
            if ([[keyCommand input] isEqualToString:[NSString stringWithFormat:@"%d", i]]) {
                [[self webViewController] switchToTab:[NSNumber numberWithInt:(i == 0 ? 9 : i - 1)]];
                return;
            }
        }
    }
    
    if ([self webViewController] && [[self webViewController] curWebViewTab])
        [[[self webViewController] curWebViewTab] handleKeyCommand:keyCommand];
}

- (UIViewController *)topViewController
{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
        return rootViewController;
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

- (void)initializeDefaults
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *plistPath = [[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"InAppSettings.bundle"] stringByAppendingPathComponent:@"Root.inApp.plist"];
    NSDictionary *settingsDictionary = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    for (NSDictionary *pref in [settingsDictionary objectForKey:@"PreferenceSpecifiers"]) {
        NSString *key = [pref objectForKey:@"Key"];
        if (key == nil)
            continue;
        
        if ([userDefaults objectForKey:key] == NULL) {
            NSObject *val = [pref objectForKey:@"DefaultValue"];
            if (val == nil)
                continue;
            
            [userDefaults setObject:val forKey:key];
        }
    }
    
    if (![userDefaults synchronize]) {
        NSLog(@"[AppDelegate] failed saving preferences");
        abort();
    }
    
    _searchEngines = [NSMutableDictionary dictionaryWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"SearchEngines.plist"]];
}

@end
