//
//  AddAlbumPasscodeVC.swift
//  Security
//
//  Created by User543 on 21.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView
import AudioToolbox

class AddAlbumPasscodeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LGAlertViewDelegate, UITextFieldDelegate {
    
    //Variables
    var album: Album!
    var typeAlbum = TypeAlbum.addAlbum
    var isSwitchOn = false

    @IBOutlet weak var addAlbumPasscodeTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addAlbumPasscodeTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
        navigationItem.title = "ALBUM SECURITY"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AddAlbumPasscodeCell") as? AddAlbumPasscodeCell {
            if typeAlbum == .editAlbum {
                let passcode = album.albumPasscode
                if passcode != "" {
                    isSwitchOn = true
                }
            }
            cell.updateViews(isSwitchOn: isSwitchOn)
            cell.switchPass.addTarget(self, action: #selector(showAlertForPasscode(_:)), for: .valueChanged)
            return cell
        } else {
            return AddAlbumPasscodeCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlertForPasscode(_ sender: UISwitch) {
        switch sender.isOn {
        case true:
            let alert = LGAlertView(textFieldsAndTitle: "Lock album", message: "Select a passcode for the album", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField, bool: true)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Lock", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.savePasscode(textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        case false:
            if typeAlbum == .editAlbum {
                let alert = LGAlertView(textFieldsAndTitle: "Disable lock", message: "Enter the album's passcode to disable the lock", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                    self.setupTextField(textField: textField, bool: false)
                }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Disable lock", delegate: self)
                alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.unlockPasscode(textField: alert.textFieldsArray?.first as! UITextField)}
                showAlertWithTextField(alert: alert)
            } else {
                album.albumPasscode = ""
                NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            }
        }
    }
    
    func showAlertWithTextField(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4045503438, blue: 0.9838915467, alpha: 1)
        alert.showAnimated()
    }
    
    func setupTextField(textField: UITextField, bool: Bool) {
        textField.delegate = self
        textField.isSecureTextEntry = true
    }
    
    func savePasscode(textField: UITextField) {
        if textField.text != "" {
            album.albumPasscode = textField.text
        } else {
            album.albumPasscode = ""
        }
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
    
    func unlockPasscode(textField: UITextField) {
        if textField.text != nil && textField.text == album.albumPasscode {
            album.albumPasscode = ""
            isSwitchOn = false
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the album's passcode to disable the lock", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField, bool: false)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Disable lock", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.unlockPasscode(textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
            isSwitchOn = true
        }
        addAlbumPasscodeTable.reloadData()
    }
}
