//
//  AddAlbumVC.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView
import QBImagePickerController

class AddAlbumVC: UIViewController, LGAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, QBImagePickerControllerDelegate, UITextFieldDelegate  {
    
    //Variables
    var album: Album!
    var typeAlbum = TypeAlbum.addAlbum
    var uiImagePicker: UIImagePickerController?
    var qbImagePicker: QBImagePickerController?
    var isAddAlbumAppear = true
    
    //Outlets
    @IBOutlet weak var albumCoverImageView: UIImageView!
    @IBOutlet weak var albumNameTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(typeAlbum: typeAlbum)
        setupPlaceholder()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if typeAlbum == .addAlbum {
            if isAddAlbumAppear {
                album = Album.mr_createEntity()
                isAddAlbumAppear = false
            }
        } else {
            if isAddAlbumAppear {
                if let image = album.albumCoverImage {
                    albumCoverImageView.image = image as? UIImage
                }
                albumNameTxt.text = album.albumName
                isAddAlbumAppear = false
            }
        }
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        if typeAlbum == .addAlbum {
            album.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        if checkSubscription() {
            album.albumName = albumNameTxt.text
            album.albumCoverImage = albumCoverImageView.image
            if album.albumPasscode == nil {
                album.albumPasscode = ""
            }
            let uuid = NSUUID().uuidString
            let localPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(uuid).path
            do {
                try FileManager.default.createDirectory(atPath: localPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
            album.pathAlbum = uuid
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setNavigationBar(typeAlbum: TypeAlbum) {
        switch typeAlbum {
        case .editAlbum:
            editAlbumNavigation()
        case .addAlbum:
            addAlbumNavigation()
        }
    }
    
    func editAlbumNavigation() {
        navigationItem.title = "EDIT ALBUM"
        setupNavigationBar()
    }
    
    func addAlbumNavigation() {
        navigationItem.title = "ADD ALBUM"
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2039215686, green: 0.3843137255, blue: 0.5176470588, alpha: 1)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func backButton() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addPhotoBtnPressed(_ sender: Any) {
        let alert = LGAlertView(title: nil, message: nil, style: LGAlertViewStyle.actionSheet, buttonTitles: ["Photo Library", "Camera"], cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, delegate: self)
        self.showAlertWithThreeButtons(alert: alert)
    }
    
    func showAlertWithThreeButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
    
    func alertView(_ alertView: LGAlertView, clickedButtonAt index: UInt, title: String?) {
        if index == 0 {
            takePhotoFromLibrary()
        } else {
            takePhotoFromCamera()
        }
    }
    
    func takePhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            qbImagePicker = QBImagePickerController()
            qbImagePicker?.delegate = self
            qbImagePicker?.mediaType = .image
            qbImagePicker?.allowsMultipleSelection = true
            qbImagePicker?.maximumNumberOfSelection = 1
            qbImagePicker?.showsNumberOfSelectedAssets = true
            self.present(qbImagePicker!, animated: true, completion: nil)
        }
    }

    func takePhotoFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            uiImagePicker = UIImagePickerController()
            uiImagePicker?.delegate = self
            uiImagePicker?.sourceType  = .camera
            uiImagePicker?.allowsEditing = false
            self.present(uiImagePicker!, animated: true, completion: nil)
        }
    }
    
    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        let phAssets = assets as! [PHAsset]
        for i in 0..<phAssets.count {
            autoreleasepool {
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                option.isSynchronous = true
                manager.requestImage(for: phAssets[i], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFit, options: option, resultHandler: {(thumbImage, info)->Void in
                    if let thumbImage = thumbImage {
                        self.albumCoverImageView.image = thumbImage
                    }
                })
            }
        }
        imagePickerController.dismiss(animated: true, completion: nil)
        qbImagePicker = nil
    }

    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        imagePickerController.dismiss(animated: true, completion: nil)
        qbImagePicker = nil
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        picker.dismiss(animated: true, completion: nil)
        uiImagePicker = nil
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if error == nil {
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            let fetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
            if let phAsset = fetchResult.lastObject {
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                option.isSynchronous = true
                manager.requestImage(for: phAsset, targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFit, options: option, resultHandler: {(thumbImage, info)->Void in
                    if let thumbImage = thumbImage {
                        self.albumCoverImageView.image = thumbImage
                    }
                })
            }
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        uiImagePicker = nil
    }
    
    @IBAction func passcodeBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let addAlbumPasscodeVC = storyboard.instantiateViewController(withIdentifier: ADD_ALBUM_PASSCODE_VC) as? AddAlbumPasscodeVC {
            addAlbumPasscodeVC.album = album
            addAlbumPasscodeVC.typeAlbum = typeAlbum
            self.navigationController?.pushViewController(addAlbumPasscodeVC, animated: true)
        }
    }
    
    func setupPlaceholder() {
        albumNameTxt.attributedPlaceholder = NSAttributedString(string: "Album name", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
    }
    
    func showAlertFreeTrial() {
        let alert = LGAlertView(title: "Start Your Free Trial", message: "You can instantly enjoy free Premium Security to create unlimited albums at any time", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "NO, THANKS", destructiveButtonTitle: "CONTINUE", delegate: self)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.buySubscription()}
        alert.didDismissAfterCancelHandler = { [unowned self](alertView: LGAlertView) in self.cancelSubscription()}
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func checkSubscription() -> Bool {
        if !IAPHelper.instance.isSubscribed, Album.mr_findAll()!.count > 5 {
            showAlertFreeTrial()
            return false
        }
        return true
    }
    
    func buySubscription() {
        IAPHelper.instance.purchase(.autoRenewablePurchase, vc: self) { (success) in
            if success {
                print("Success buy subscription")
            } else {
                print("No success buy subscription")
            }
        }
    }
    
    func cancelSubscription() {
        if typeAlbum == .addAlbum {
            album.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Keyboard control
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}


