//
//  AntiTheftVC.swift
//  Security
//
//  Created by User543 on 05.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import LGAlertView
import AVFoundation
import CoreMotion

class AntiTheftVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LGAlertViewDelegate{
    
    //Variables
    let motionManager = CMMotionManager()
    var motionIndicator: Bool!
    var powerIndicator: Bool!
    var headsetIndicator: Bool!
    
    @IBOutlet weak var antiTheftTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        antiTheftTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        motionIndicator = UserDefaults.standard.bool(forKey: "motion")
        powerIndicator = UserDefaults.standard.bool(forKey: "power")
        headsetIndicator = UserDefaults.standard.bool(forKey: "headset")
        antiTheftTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getAntiTheft().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AntiTheftCell") as? AntiTheftCell {
            let antiTheft = DataService.instance.getAntiTheft()[indexPath.row]
            cell.updateViews(antiTheft: antiTheft)
            if indexPath.row == 0 {
                if motionIndicator {
                    print("TRECUT PRIN TRUE motionIndicator")
                   cell.statusIndicator.backgroundColor = #colorLiteral(red: 0.4470588235, green: 0.7176470588, blue: 0.4823529412, alpha: 1)
                } else {
                    print("TRECUT PRIN False motionIndicator")
                    cell.statusIndicator.backgroundColor = #colorLiteral(red: 1, green: 0.337254902, blue: 0.3411764706, alpha: 1)
                }
            }
            if indexPath.row == 1 {
                if powerIndicator {
                  cell.statusIndicator.backgroundColor = #colorLiteral(red: 0.4470588235, green: 0.7176470588, blue: 0.4823529412, alpha: 1)
                } else {
                    cell.statusIndicator.backgroundColor = #colorLiteral(red: 1, green: 0.337254902, blue: 0.3411764706, alpha: 1)
                }
            }
            if indexPath.row == 2 {
                if headsetIndicator {
                  cell.statusIndicator.backgroundColor = #colorLiteral(red: 0.4470588235, green: 0.7176470588, blue: 0.4823529412, alpha: 1)
                } else {
                    cell.statusIndicator.backgroundColor = #colorLiteral(red: 1, green: 0.337254902, blue: 0.3411764706, alpha: 1)
                }
            }
            return cell
        } else {
            return AntiTheftCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let alert = LGAlertView(title: "Activate Alarm?", message: "The alarm will go off if the phone is moved. You will need to enter your PIN to turn off the alarm.\n\nMake sure your Ringer volume is turned up and your device is not on Silent.", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Activate", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.activateMotion()}
            showAlertWithTwoButtons(alert: alert)
            break
        case 1:
            let alert = LGAlertView(title: "Activate Alarm?", message: "The alarm will go off if the charger is unplugged. You will need to enter your PIN to turn off the alarm.\n\nMake sure your Ringer volume is turned up and your device is not on Silent.", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Activate", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.activatePower()}
            showAlertWithTwoButtons(alert: alert)
            break
        case 2:
            let alert = LGAlertView(title: "Activate Alarm?", message: "The alarm will go off if the headset is unplugged. You will need to enter your PIN to turn off the alarm.\n\nMake sure your Ringer volume is turned up and your device is not on Silent.", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Activate", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.activateHeadset()}
            showAlertWithTwoButtons(alert: alert)
            break
        default:
            break
        }
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlertWithTwoButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
    
    func activateMotion() {
        let cell = antiTheftTable.cellForRow(at: IndexPath(row: 0, section: 0)) as! AntiTheftCell
        cell.statusIndicator.backgroundColor = #colorLiteral(red: 0.4470588235, green: 0.7176470588, blue: 0.4823529412, alpha: 1)
        UserDefaults.standard.set(true, forKey: "motion")
        motionIndicator = true
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name.init("motion"), object: nil)
        antiTheftTable.reloadData()
    }
    
    func activatePower() {
        let cell = antiTheftTable.cellForRow(at: IndexPath(row: 1, section: 0)) as! AntiTheftCell
        cell.statusIndicator.backgroundColor = #colorLiteral(red: 0.4470588235, green: 0.7176470588, blue: 0.4823529412, alpha: 1)
        UserDefaults.standard.set(true, forKey: "power")
        powerIndicator = true
        UserDefaults.standard.synchronize()
        UIDevice.current.isBatteryMonitoringEnabled = true
        antiTheftTable.reloadData()
    }
    
    func activateHeadset() {
        let cell = antiTheftTable.cellForRow(at: IndexPath(row: 2, section: 0)) as! AntiTheftCell
        cell.statusIndicator.backgroundColor = #colorLiteral(red: 0.4470588235, green: 0.7176470588, blue: 0.4823529412, alpha: 1)
        UserDefaults.standard.set(true, forKey: "headset")
        headsetIndicator = true
        UserDefaults.standard.synchronize()
        AVAudioSession.sharedInstance()
        antiTheftTable.reloadData()
    }
}
