//
//  ReportsCell.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class ReportsCell: UITableViewCell {

    @IBOutlet weak var imageName: UIImageView!
    @IBOutlet weak var reportName: UILabel!
    @IBOutlet weak var info: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    
    func updateView(report: Reports, count: Int) {
        imageName.image = UIImage(named: report.imageName)
        reportName.text = report.title
        if count == 0 {
            info.text = report.info
        } else {
            info.text = "\(count) new entries"
        }
    }
}
