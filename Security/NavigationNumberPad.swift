//
//  NavigationNumberPad.swift
//  Security
//
//  Created by User543 on 08.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

struct NavigationNumberPad {
    private(set) public var title: String
    private(set) public var nameImageLeftBtn: String
    private(set) public var nameImageRightBtn: String
    private(set) public var titleLeftBtn: String
    private(set) public var titleRightBtn: String
    private(set) public var emptyLeftBtn: Bool
    private(set) public var emptyRightBtn: Bool
    
    init(title: String, nameImageLeftBtn: String, nameImageRightBtn: String, titleLeftBtn: String, titleRightBtn: String, emptyLeftBtn: Bool, emptyRightBtn: Bool) {
        self.title = title
        self.nameImageLeftBtn = nameImageLeftBtn
        self.nameImageRightBtn = nameImageRightBtn
        self.titleLeftBtn = titleLeftBtn
        self.titleRightBtn = titleRightBtn
        self.emptyLeftBtn = emptyLeftBtn
        self.emptyRightBtn = emptyRightBtn
    }
}
