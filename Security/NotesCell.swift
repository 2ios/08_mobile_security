//
//  NotesCell.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class NotesCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lockedNote: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }

    func updateViews(note: Note) {
        if note.noteName == "" {
            title.text = "No title"
        } else {
            title.text = note.noteName
        }
        if note.notePasscode != "" {
            lockedNote.isHidden = false
        } else {
            lockedNote.isHidden = true
        }
    }
}
