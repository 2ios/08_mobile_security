//
//  MoreCellView.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

//@IBDesignable
class MoreCellView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = cornerRadius
    }
    
    func headerCellView(section: Int, headerHeight: CGFloat) -> UIView {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        let headerStr = DataService.instance.getHeaderMore()[section]
        let titleLbl = UILabel()
        titleLbl.text = headerStr
        titleLbl.font = UIFont(name: "Roboto-Regular", size: 20)
        titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        titleLbl.sizeToFit()
        titleLbl.frame.origin = CGPoint(x: 10, y: headerHeight / 2 - titleLbl.frame.height / 2)
        headerView.addSubview(titleLbl)
        return headerView
    }
    
    func headerSettingsAntiTheftCellView(section: Int, headerHeight: CGFloat) -> UIView {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        let headerStr = DataService.instance.getHeaderSettingsAntiTheft()[section]
        let titleLbl = UILabel()
        titleLbl.text = headerStr
        titleLbl.font = UIFont(name: "Roboto-Regular", size: 20)
        titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        titleLbl.sizeToFit()
        titleLbl.frame.origin = CGPoint(x: 10, y: headerHeight / 2 - titleLbl.frame.height / 2)
        headerView.addSubview(titleLbl)
        return headerView
    }
}
