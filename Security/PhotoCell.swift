//
//  PhotoCell.swift
//  Security
//
//  Created by User543 on 25.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var isSelectedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateViews(image: UIImage?) {
        imageView.image = image
    }
    
//    override var isSelected: Bool {
//        didSet {
////            self.layer.borderWidth = 3.0
////            self.layer.borderColor = isSelected ? UIColor.blue.cgColor : UIColor.clear.cgColor
//            isSelectedImageView.isHidden = isSelected ? false : true
//        }
//    }
}
