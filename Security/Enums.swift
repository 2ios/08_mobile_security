//
//  Enums.swift
//  Security
//
//  Created by User543 on 22.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

enum TypeAlbum {
    case addAlbum
    case editAlbum
}

enum TypeNote {
    case addNote
    case editNote
    case showNote
}

enum TypeContact {
    case addContact
    case editContact
    case showContact
}

enum TypePassword {
    case addPassword
    case editPassword
    case showPassword
}

enum TypeCollectionImages {
    case showImages
    case addImages
}

@objc enum TypeReports: Int {
    case showFromSafeBrowser
    case showReports
}

@objc enum TypeNumberPad: Int {
    case checkPassword
    case newPassword
    case repeatNewPassword
    case alert
    case foreground
}
