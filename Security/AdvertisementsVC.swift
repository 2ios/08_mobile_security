//
//  AdvertisementsVC.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AdvertisementsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var advertisementsTable: UITableView!
    
    var advertisementsDict = NSDictionary()
    var keyAdvertisementsDict = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        advertisementsTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
        if let dict = UserDefaults.standard.object(forKey: "applicableURLBlockerTargets") as? NSDictionary {
            advertisementsDict = dict
            keyAdvertisementsDict = advertisementsDict.allKeys as! [String]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return advertisementsDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementsCell") as? AdvertisementsCell {
            let title = keyAdvertisementsDict[indexPath.row]
            let date = advertisementsDict[title]
            let advertisements = Advertisements(title: title as! String, date: date as! String)
            cell.updateViews(advertisements: advertisements)
            return cell
        } else {
            return AdvertisementsCell()
        }
    }

    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clearBtnPressed(_ sender: Any) {
        advertisementsDict = [:]
        advertisementsTable.reloadData()
        UserDefaults.standard.set(advertisementsDict, forKey: "applicableURLBlockerTargets")
        UserDefaults.standard.synchronize()
    }
}
