//
//  VaultCell.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData

class VaultCell: UITableViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    
    func updateViews(category: CategoryVault, index: Int) {
        categoryImage.image = UIImage(named: category.imageName)
        categoryName.text = category.title
        switch index {
        case 0:
            if let count = Album.mr_findAll()?.count {
                if count > 0 {
                    categoryInfo.text = "\(count) Albums"
                } else {
                    categoryInfo.text = category.info
                }
            }
        case 1:
            if let count = Note.mr_findAll()?.count {
                if count > 0 {
                    categoryInfo.text = "\(count) Notes"
                } else {
                    categoryInfo.text = category.info
                }
            }
        case 2:
            if let count = Contact.mr_findAll()?.count {
                if count > 0 {
                    categoryInfo.text = "\(count) Contacts"
                } else {
                    categoryInfo.text = category.info
                }
            }
        case 3:
            if let count = Password.mr_findAll()?.count {
                if count > 0 {
                    categoryInfo.text = "\(count) Passwords"
                } else {
                    categoryInfo.text = category.info
                }
            }
        default:
            break
        }
    }
}
