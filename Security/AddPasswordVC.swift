//
//  AddPasswordVC.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView

class AddPasswordVC: UIViewController, LGAlertViewDelegate, UITextFieldDelegate {
    
    //Variables
    var password: Password!
    var typePassword = TypePassword.addPassword
    var isAddPasswordAppear = true
    
    //Outlets
    @IBOutlet weak var passwordTitleTxt: UITextField!
    @IBOutlet weak var passwordUsernameTxt: UITextField!
    @IBOutlet weak var passwordPasswordTxt: UITextField!
    @IBOutlet weak var passwordWebsiteTxt: UITextField!
    @IBOutlet weak var passwordNotesTxt: UITextField!
    @IBOutlet weak var passcodeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(typePassword: typePassword)
        setupPlaceholder()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if typePassword == .addPassword {
            if isAddPasswordAppear {
               password = Password.mr_createEntity()
                isAddPasswordAppear = false
            }
        } else {
            if isAddPasswordAppear {
                passwordTitleTxt.text = password.passwordTitle
                passwordUsernameTxt.text = password.passwordUsername
                passwordPasswordTxt.text = password.passwordPassword
                passwordWebsiteTxt.text = password.passwordWebsite
                passwordNotesTxt.text = password.passwordNotes
                if typePassword == .showPassword {
                    passwordTitleTxt.isUserInteractionEnabled = false
                    passwordUsernameTxt.isUserInteractionEnabled = false
                    passwordPasswordTxt.isUserInteractionEnabled = false
                    passwordWebsiteTxt.isUserInteractionEnabled = false
                    passwordNotesTxt.isUserInteractionEnabled = false
                    passcodeView.isHidden = true
                }
                isAddPasswordAppear = false
            }
        }
    }

    @IBAction func navBackBtnPressed(_ sender: Any) {
        if typePassword == .addPassword {
            password.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }

    @IBAction func doneBtnPressed(_ sender: Any) {
        if checkSubscription() {
            if password.passwordPasscode == nil {
                password.passwordPasscode = ""
            }
            password.passwordTitle = passwordTitleTxt.text
            password.passwordUsername = passwordUsernameTxt.text
            password.passwordPassword = passwordPasswordTxt.text
            password.passwordWebsite = passwordWebsiteTxt.text
            password.passwordNotes = passwordNotesTxt.text
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setNavigationBar(typePassword: TypePassword) {
        switch typePassword {
        case .editPassword:
            editPasswordNavigation()
        case .showPassword:
            showPasswordNavigation()
        case .addPassword:
            addPasswordNavigation()
        }
    }
    
    func editPasswordNavigation() {
        navigationItem.title = "EDIT PASSWORD"
        setupNavigationBar()
    }
    
    func addPasswordNavigation() {
        navigationItem.title = "ADD PASSWORD"
        setupNavigationBar()
    }
    
    func showPasswordNavigation() {
        if password.passwordTitle == nil {
            navigationItem.title = "No title"
        } else {
            navigationItem.title = password.passwordTitle
        }
        setupNavigationBar()
        navigationItem.rightBarButtonItem = UIBarButtonItem()
        let leftBtn = UIButton(type: .custom)
        leftBtn.addTarget(self, action: #selector(backButton), for: .touchUpInside)
        if let image = UIImage(named: "nav_back_button") {
            leftBtn.setImage(image, for: .normal)
            leftBtn.frame.size = CGSize(width: image.size.width, height: image.size.height)
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBtn)
    }

    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2039215686, green: 0.3843137255, blue: 0.5176470588, alpha: 1)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func backButton() {
        navigationController?.popViewController(animated: true)
    }

    func setupPlaceholder() {
        passwordTitleTxt.attributedPlaceholder = NSAttributedString(string: "Title", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        passwordUsernameTxt.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        passwordPasswordTxt.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        passwordWebsiteTxt.attributedPlaceholder = NSAttributedString(string: "Website", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
        passwordNotesTxt.attributedPlaceholder = NSAttributedString(string: "Notes", attributes: [NSForegroundColorAttributeName: #colorLiteral(red: 0.5511015654, green: 0.6069272161, blue: 0.6694045663, alpha: 1)])
    }
    
    func showAlertFreeTrial() {
        let alert = LGAlertView(title: "Start Your Free Trial", message: "You can instantly enjoy free Premium Security to create unlimited passwords at any time", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "NO, THANKS", destructiveButtonTitle: "CONTINUE", delegate: self)
        alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.buySubscription()}
        alert.didDismissAfterCancelHandler = { [unowned self](alertView: LGAlertView) in self.cancelSubscription()}
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.5764705882, blue: 0.1098039216, alpha: 1)
        alert.showAnimated()
    }
    
    func checkSubscription() -> Bool {
        if !IAPHelper.instance.isSubscribed, Password.mr_findAll()!.count > 5 {
            showAlertFreeTrial()
            return false
        }
        return true
    }
    
    func buySubscription() {
        IAPHelper.instance.purchase(.autoRenewablePurchase, vc: self) { (success) in
            if success {
                print("Success buy subscription")
            } else {
                print("No success buy subscription")
            }
        }
    }
    
    func cancelSubscription() {
        if typePassword == .addPassword {
            password.mr_deleteEntity()
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func passcodeBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let addPasswordPasscodeVC = storyboard.instantiateViewController(withIdentifier: ADD_PASSWORD_PASSCODE_VC) as? AddPasswordPasscodeVC {
            addPasswordPasscodeVC.password = password
            addPasswordPasscodeVC.typePassword = typePassword
            self.navigationController?.pushViewController(addPasswordPasscodeVC, animated: true)
        }
    }
    
    // MARK: Keyboard control
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}






