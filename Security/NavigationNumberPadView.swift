//
//  NavigationNumberPadView.swift
//  Security
//
//  Created by User543 on 08.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class NavigationNumberPadView: UINavigationItem {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateViews(navigation: NavigationNumberPad, selectorLeftBtn: Selector?, selectorRightBtn: Selector?, target: Any?) {
        self.title = navigation.title
        if navigation.emptyLeftBtn {
            self.leftBarButtonItem = UIBarButtonItem()
        } else {
            let btn = UIButton(type: .custom)
            if let selectLBtn = selectorLeftBtn {
                  btn.addTarget(target, action: selectLBtn, for: .touchUpInside)
            }
            if navigation.titleLeftBtn.isEmpty {
                if let image = UIImage(named: navigation.nameImageLeftBtn) {
                    btn.setImage(image, for: UIControlState.normal)
                    btn.frame.size = CGSize(width: image.size.width, height: image.size.height)
                }
            } else {
                btn.setTitle(navigation.titleLeftBtn, for: .normal)
                btn.sizeToFit()
            }
            self.leftBarButtonItem = UIBarButtonItem(customView: btn)
        }
        if navigation.emptyRightBtn {
            self.rightBarButtonItem = UIBarButtonItem()
        } else {
            let btn = UIButton(type: .custom)
            if let selectRBtn = selectorRightBtn {
               btn.addTarget(target, action: selectRBtn, for: .touchUpInside)
            }
            if navigation.titleRightBtn.isEmpty {
                if let image = UIImage(named: navigation.nameImageRightBtn) {
                    btn.setImage(image, for: UIControlState.normal)
                    btn.frame.size = CGSize(width: image.size.width, height: image.size.height)
                }
            } else {
                btn.setTitle(navigation.titleRightBtn, for: .normal)
                btn.sizeToFit()
            }
            self.rightBarButtonItem = UIBarButtonItem(customView: btn)
        }
    }
}
