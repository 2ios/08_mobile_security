//
//  DataServices.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

class DataService {
    
    static let instance = DataService()
    
    private let categoriesVault = [
        CategoryVault(imageName: "albums_ico", title: "Albums", info: "No Albums"),
        CategoryVault(imageName: "notes_ico", title: "Notes", info: "No Notes"),
        CategoryVault(imageName: "contacts_ico", title: "Contacts", info: "No Contacts"),
        CategoryVault(imageName: "password_ico", title: "Passwords", info: "No Passwords")
    ]
    
    func getCategoriesVault() -> [CategoryVault] {
        return categoriesVault
    }
    
    private let seguesVault = [
        TO_ALBUMS,
        TO_NOTES,
        TO_CONTACTS,
        TO_PASSWORDS
    ]
    
    func getSeguesVault() -> [String] {
        return seguesVault
    }
    
    private let more = [
        [
//            More(imageName: "faq_item", title: "FAQ"),
            More(imageName: "restore_item", title: "Restore Security")
        ],
        
        [
            More(imageName: "tos_item", title: "Terms of Use"),
            More(imageName: "privacy_policy_item", title: "Privacy Policy"),
            More(imageName: "subscription_info_item", title: "Subscription Info")
        ]
        
    ]
    
    func getMore() -> [[More]] {
        return more
    }
    
    private let headerMore = [
        "SUPPORT",
        "OTHER"
    ]
    
    func getHeaderMore() -> [String] {
        return headerMore
    }
    
    private let reports = [
//        Reports(imageName: "malware_blocked_icon", title: "Malware Hosts Blocked", info: "No new entries"),
        Reports(imageName: "trackers_blocked_icon", title: "Trackers Blocked", info: "No new entries"),
        Reports(imageName: "ads_blocked_icon", title: "Advertisements", info: "No new entries")
    ]
    
    func getReports() -> [Reports] {
        return reports
    }
    
    private let seguesReports = [
        TO_MALWARE,
        TO_TRACKERS,
        TO_ADVERTISEMENTS
    ]
    
    func getSeguesReports() -> [String] {
        return seguesReports
    }
    
    private let malware = [
        Malware(title: "googleMalware", date: "01-02-17 18:04"),
        Malware(title: "facebookMalware", date: "02-03-17 18:05")
    ]
    
    func getMalware() -> [Malware] {
        return malware
    }
    
    private let antiTheft = [
        AntiTheft(imageName: "anti-theft-motion", title: "Motion", info: "Activate alarm when phone is moved."),
        AntiTheft(imageName: "anti-theft-power", title: "Power", info: "Activate alarm when charger is unplugged."),
        AntiTheft(imageName: "anti-theft-earphone", title: "Headset", info: "Activate alarm when earphone is unplugged.")
    ]
    
    func getAntiTheft() -> [AntiTheft] {
        return antiTheft
    }
    
    private let headerSettingsAntiTheft = [
        "PIN"
    ]
    
    func getHeaderSettingsAntiTheft() -> [String] {
        return headerSettingsAntiTheft
    }
    
    private let settingsAntiTheft = [
        SettingsAntiTheft(title: "Change PIN")
    ]
    
    func getSettingsAntiTheft() -> [SettingsAntiTheft] {
        return settingsAntiTheft
    }
}
