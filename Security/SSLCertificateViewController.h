//
//  SSLCertificateViewController.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OrderedDictionary.h"
#import "SSLCertificate.h"

@interface SSLCertificateViewController : UITableViewController <UITableViewDelegate> {
    MutableOrderedDictionary *certInfo;
}

@property (strong) SSLCertificate *certificate;

- (id)initWithSSLCertificate:(SSLCertificate *)cert;

@end
