//
//  LocalNetworkChecker.h
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalNetworkChecker : NSObject

+ (void)clearCache;
+ (BOOL)isHostOnLocalNet:(NSString *)host;

@end
