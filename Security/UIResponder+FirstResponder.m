//
//  UIResponder+FirstResponder.m
//  Security
//
//  Created by User543 on 05.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

#import "UIResponder+FirstResponder.h"

static __weak id currentFirstResponder;

@implementation UIResponder (FirstResponder)

+ (id)currentFirstResponder
{
    currentFirstResponder = nil;
    [[UIApplication sharedApplication] sendAction:@selector(findFirstResponder:) to:nil from:nil forEvent:nil];
    return currentFirstResponder;
}

- (void)findFirstResponder:(id)sender
{
    currentFirstResponder = self;
}

@end

