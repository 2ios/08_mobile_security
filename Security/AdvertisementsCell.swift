//
//  AdvertisementsCell.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AdvertisementsCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateViews(advertisements: Advertisements) {
        title.text = advertisements.title
        date.text = advertisements.date
    }

}
