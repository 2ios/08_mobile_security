//
//  AddPasswordPasscodeVC.swift
//  Security
//
//  Created by User543 on 16.10.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView
import AudioToolbox

class AddPasswordPasscodeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LGAlertViewDelegate, UITextFieldDelegate {

    //Variables
    var password: Password!
    var typePassword = TypePassword.addPassword
    var isSwitchOn = false
    
    @IBOutlet weak var addPasswordPasscodeTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPasswordPasscodeTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
        navigationItem.title = "PASSWORD SECURITY"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AddPasswordPasscodeCell") as? AddPasswordPasscodeCell {
            if typePassword == .editPassword {
                let passcode = password.passwordPasscode
                if passcode != "" {
                    isSwitchOn = true
                }
            }
            cell.updateViews(isSwitchOn: isSwitchOn)
            cell.switchPass.addTarget(self, action: #selector(showAlertForPasscode(_:)), for: .valueChanged)
            return cell
        } else {
            return AddPasswordPasscodeCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlertForPasscode(_ sender: UISwitch) {
        switch sender.isOn {
        case true:
            let alert = LGAlertView(textFieldsAndTitle: "Lock password", message: "Select a passcode for the password", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField, bool: true)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Lock", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.savePasscode(textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        case false:
            if typePassword == .editPassword {
                let alert = LGAlertView(textFieldsAndTitle: "Disable lock", message: "Enter the password's passcode to disable the lock", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                    self.setupTextField(textField: textField, bool: false)
                }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Disable lock", delegate: self)
                alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.unlockPasscode(textField: alert.textFieldsArray?.first as! UITextField)}
                showAlertWithTextField(alert: alert)
            } else {
                password.passwordPasscode = ""
                NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
            }
        }
    }
    
    func showAlertWithTextField(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4045503438, blue: 0.9838915467, alpha: 1)
        alert.showAnimated()
    }
    
    func setupTextField(textField: UITextField, bool: Bool) {
        textField.delegate = self
        textField.isSecureTextEntry = true
    }
    
    func savePasscode(textField: UITextField) {
        if textField.text != "" {
            password.passwordPasscode = textField.text
        } else {
            password.passwordPasscode = ""
        }
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
    
    func unlockPasscode(textField: UITextField) {
        if textField.text != nil && textField.text == password.passwordPasscode {
            password.passwordPasscode = ""
            isSwitchOn = false
            NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the password's passcode to disable the lock", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField, bool: false)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Disable lock", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.unlockPasscode(textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
            isSwitchOn = true
        }
        addPasswordPasscodeTable.reloadData()
    }
}
