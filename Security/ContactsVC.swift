//
//  ContactsVC.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView
import AudioToolbox

class ContactsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LGAlertViewDelegate, UITextFieldDelegate {

    //Variables
    var contacts = [Contact]()
    
    //Outlets
    @IBOutlet weak var contactsTable: UITableView!
    @IBOutlet weak var noContactsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactsTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        contacts = Contact.mr_findAll() as! [Contact]
        checkNoContacts()
        contactsTable.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell") as? ContactsCell {
            let contact = contacts[indexPath.row]
            cell.updateViews(contact: contact)
            return cell
        } else {
            return ContactsCell()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            tableView.setEditing(false, animated: true)
            let alert = LGAlertView(title: "Are you sure?", message: "Do you really want to delete this contact?", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.deleteCellFromTable(atIndex: indexPath.row)}
            self.showAlertWithTwoButtons(alert: alert)
        }
    
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            if self.contacts[indexPath.row].contactPasscode != "" {
                let alert = LGAlertView(textFieldsAndTitle: "Enter passcode", message: "Enter the contact's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                    self.setupTextField(textField: textField)
                }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
                alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscodeEdit(atIndex: indexPath.row, textField: alert.textFieldsArray?.first as! UITextField)}
                self.showAlertWithTextField(alert: alert)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let addContactVC = storyboard.instantiateViewController(withIdentifier: ADD_CONTACT_VC) as? AddContactVC {
                    addContactVC.contact = self.contacts[indexPath.row]
                    addContactVC.typeContact = TypeContact.editContact
                    let navController = UINavigationController.init(rootViewController: addContactVC)
                    self.present(navController, animated: true, completion: nil)
                }
            }
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        editAction.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
        return [deleteAction, editAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if contacts[indexPath.row].contactPasscode != "" {
            let alert = LGAlertView(textFieldsAndTitle: "Enter passcode", message: "Enter the contact's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscode(atIndex: indexPath.row, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let addContactVC = storyboard.instantiateViewController(withIdentifier: ADD_CONTACT_VC) as? AddContactVC {
                addContactVC.contact = contacts[indexPath.row]
                addContactVC.typeContact = TypeContact.showContact
                self.navigationController?.pushViewController(addContactVC, animated: true)
            }
        }
    }
    
    func showAlertWithTextField(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4045503438, blue: 0.9838915467, alpha: 1)
        alert.showAnimated()
    }
    
    func setupTextField(textField: UITextField) {
        textField.delegate = self
        textField.isSecureTextEntry = true
    }
    
    func checkPasscode(atIndex index: Int, textField: UITextField) {
        let contact = contacts[index]
        if textField.text == contact.contactPasscode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let addContactVC = storyboard.instantiateViewController(withIdentifier: ADD_CONTACT_VC) as? AddContactVC {
                addContactVC.contact = contacts[index]
                addContactVC.typeContact = .showContact
                self.navigationController?.pushViewController(addContactVC, animated: true)
            }
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the contact's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscode(atIndex: index, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        }
    }
    
    func checkPasscodeEdit(atIndex index: Int, textField: UITextField) {
        let contact = contacts[index]
        if textField.text == contact.contactPasscode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let addContactVC = storyboard.instantiateViewController(withIdentifier: ADD_CONTACT_VC) as? AddContactVC {
                addContactVC.contact = self.contacts[index]
                addContactVC.typeContact = TypeContact.editContact
                let navController = UINavigationController.init(rootViewController: addContactVC)
                self.present(navController, animated: true, completion: nil)
            }
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the contact's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscodeEdit(atIndex: index, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        }
    }
    

    func deleteCellFromTable(atIndex index: Int) {
        self.contacts.remove(at: index).mr_deleteEntity()
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        contactsTable.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        contactsTable.reloadData()
        checkNoContacts()
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func checkNoContacts() {
        if contacts.count > 0 {
            noContactsView.isHidden = true
        } else {
            noContactsView.isHidden = false
        }
    }
    
    func showAlertWithTwoButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
}

