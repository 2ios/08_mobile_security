//
//  Reports.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

struct Reports {
    private(set) public var imageName: String
    private(set) public var title: String
    private(set) public var info: String
    
    init (imageName: String, title: String, info: String) {
        self.imageName = imageName
        self.title = title
        self.info = info
    }
}
