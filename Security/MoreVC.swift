//
//  MoreVC.swift
//  Security
//
//  Created by User543 on 07.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class MoreVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var moreTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moreTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return DataService.instance.getHeaderMore().count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getMore()[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = DataService.instance.getMore()[indexPath.section]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell") as? MoreCell {
            let more = section[indexPath.row]
            cell.updateViews(more: more)
            return cell
        } else {
            return MoreCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let webVC = storyboard.instantiateViewController(withIdentifier: WEB_VC) as? WebVC {
            print(indexPath.section)
            print(indexPath.row)
            switch indexPath.section {
//            case 0:
//                if indexPath.row == 0 {
//                    webVC.item = 0
//                    webVC.navTitle = "FAQ"
//                }
            case 1:
                if indexPath.row == 0 {
                    webVC.item = 0
                    webVC.navTitle = "TERMS OF USE"
                }
                if indexPath.row == 1 {
                    webVC.item = 1
                    webVC.navTitle = "PRIVACY POLICY"
                }
                if indexPath.row == 2 {
                    webVC.item = 2
                    webVC.navTitle = "SUBSCRIPTION INFO"
                }
            default:
                break
            }
            if indexPath.section == 0 && indexPath.row == 0 {
                print("Restore subscription")
                restoreSubscription()
            } else {
              self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: MoreCellView! = MoreCellView()
        let headerHeight: CGFloat = 35
        return headerView.headerCellView(section: section, headerHeight: headerHeight)
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func restoreSubscription() {
        IAPHelper.instance.restorePurchases(vc: self) { (success) in
            if success {
                print("Restore completed")
            } else {
                print("Restore uncompleted")
            }
        }
    }
}
