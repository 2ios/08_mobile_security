//
//  AlbumsVC.swift
//  Security
//
//  Created by User543 on 06.09.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import LGAlertView
import AudioToolbox

class AlbumsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LGAlertViewDelegate, UITextFieldDelegate {
    
    //Variables
    var albums = [Album]()
    
    //Outlets
    @IBOutlet weak var albumsTable: UITableView!
    @IBOutlet weak var noAlbumsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        albumsTable.tableFooterView = UIView()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Black", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        albums = Album.mr_findAll() as! [Album]
        checkNoAlbums()
        albumsTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumsCell") as? AlbumsCell {
            let album = albums[indexPath.row]
            cell.updateViews(album: album)
            return cell
        } else {
            return AlbumsCell()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            tableView.setEditing(false, animated: true)
            let alert = LGAlertView(title: "Are you sure?", message: "Do you really want to delete this album?", style: LGAlertViewStyle.alert, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.deleteCellFromTable(atIndex: indexPath.row)}
            self.showAlertWithTwoButtons(alert: alert)
        }
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            if self.albums[indexPath.row].albumPasscode != "" {
                let alert = LGAlertView(textFieldsAndTitle: "Enter passcode", message: "Enter the album's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                    self.setupTextField(textField: textField)
                }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
                alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscodeEdit(atIndex: indexPath.row, textField: alert.textFieldsArray?.first as! UITextField)}
                self.showAlertWithTextField(alert: alert)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let addAlbumVC = storyboard.instantiateViewController(withIdentifier: ADD_ALBUM_VC) as? AddAlbumVC {
                    addAlbumVC.album = self.albums[indexPath.row]
                    addAlbumVC.typeAlbum = TypeAlbum.editAlbum
                    let navController = UINavigationController.init(rootViewController: addAlbumVC)
                    self.present(navController, animated: true, completion: nil)
                }
            }
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        editAction.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
        return [deleteAction, editAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if albums[indexPath.row].albumPasscode != "" {
            let alert = LGAlertView(textFieldsAndTitle: "Enter passcode", message: "Enter the album's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscode(atIndex: indexPath.row, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let photoVC = storyboard.instantiateViewController(withIdentifier: PHOTO_VC) as? PhotosVC {
                photoVC.album = albums[indexPath.row]
                photoVC.typeCollectionImages = .showImages
                self.navigationController?.pushViewController(photoVC, animated: true)
            }
        }
    }

    @IBAction func navBackBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func deleteCellFromTable(atIndex index: Int) {
        let path = albums[index].pathAlbum!
        let FileToDelete = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(path)
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(atPath: FileToDelete.path)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        self.albums.remove(at: index).mr_deleteEntity()
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        albumsTable.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        albumsTable.reloadData()
        checkNoAlbums()
    }
    
    func checkNoAlbums() {
        if albums.count > 0 {
            noAlbumsView.isHidden = true
        } else {
            noAlbumsView.isHidden = false
        }
    }
    
    func showAlertWithTwoButtons(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4116952419, blue: 1, alpha: 1)
        alert.showAnimated()
    }
    
    func showAlertWithTextField(alert: LGAlertView) {
        alert.destructiveButtonTitleColor = #colorLiteral(red: 0, green: 0.4045503438, blue: 0.9838915467, alpha: 1)
        alert.showAnimated()
    }
    
    func setupTextField(textField: UITextField) {
        textField.delegate = self
        textField.isSecureTextEntry = true
    }
    
    func checkPasscode(atIndex index: Int, textField: UITextField) {
        let album = albums[index]
        if textField.text == album.albumPasscode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let photoVC = storyboard.instantiateViewController(withIdentifier: PHOTO_VC) as? PhotosVC {
                photoVC.album = albums[index]
                photoVC.typeCollectionImages = .showImages
                self.navigationController?.pushViewController(photoVC, animated: true)
            }
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the album's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscode(atIndex: index, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        }
    }
    
    func checkPasscodeEdit(atIndex index: Int, textField: UITextField) {
        let album = albums[index]
        if textField.text == album.albumPasscode {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let addAlbumVC = storyboard.instantiateViewController(withIdentifier: ADD_ALBUM_VC) as? AddAlbumVC {
                addAlbumVC.album = self.albums[index]
                addAlbumVC.typeAlbum = TypeAlbum.editAlbum
                let navController = UINavigationController.init(rootViewController: addAlbumVC)
                self.present(navController, animated: true, completion: nil)
            }
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let alert = LGAlertView(textFieldsAndTitle: "Try Again", message: "Enter the album's passcode to open it.", numberOfTextFields: 1, textFieldsSetupHandler: { (textField, index) in
                self.setupTextField(textField: textField)
            }, buttonTitles: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Enter", delegate: self)
            alert.didDismissAfterDestructiveHandler = { [unowned self](alertView: LGAlertView) in self.checkPasscodeEdit(atIndex: index, textField: alert.textFieldsArray?.first as! UITextField)}
            showAlertWithTextField(alert: alert)
        }
    }
}
